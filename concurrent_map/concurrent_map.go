package concurrent_map

import (
	"fmt"

	cmap "github.com/orcaman/concurrent-map"
)

type ConcurrentMap struct {
	cm cmap.ConcurrentMap
}

func NewConcurrentMap() *ConcurrentMap {
	return &ConcurrentMap{
		cm: cmap.New(),
	}
}

func (c *ConcurrentMap) InsertUint64(key uint64, value interface{}) {
	c.cm.Set(fmt.Sprint(key), value)
}
func (c *ConcurrentMap) InsertIntoSlice(key string, value uint64) {
	slice, ok := c.cm.Get(key)
	if !ok {
		c.cm.Set(key, []uint64{value})
		return
		// c.cm.Set(key, value)
	}

	c.cm.Set(fmt.Sprint(key), append(slice.([]uint64), value))
}

func (c *ConcurrentMap) InsertUint64IntoSlice(key uint64, value uint64) {
	slice, ok := c.cm.Get(fmt.Sprint(key))
	if !ok {
		c.cm.Set(fmt.Sprint(key), []uint64{value})
		return
		// c.cm.Set(key, value)
	}

	c.cm.Set(fmt.Sprint(key), append(slice.([]uint64), value))
}

func (c *ConcurrentMap) ReadUint64(key uint64) (interface{}, bool) {
	val, ok := c.cm.Get(fmt.Sprint(key))
	if !ok {
		return nil, false
	}
	return val, true
}
func (c *ConcurrentMap) ReadUint64Slice(key uint64) ([]uint64, bool) {
	val, ok := c.cm.Get(fmt.Sprint(key))
	if !ok {
		return nil, false
	}
	return val.([]uint64), true
}

func (c *ConcurrentMap) ReadSlice(key string) ([]uint64, bool) {
	val, ok := c.cm.Get(key)
	if !ok {
		return nil, false
	}
	return val.([]uint64), true
}
