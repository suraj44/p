package store

type Error string

func (e Error) Error() string { return string(e) }

const (
	errDirtyRead = Error("Dirty value. No consistent value present for this key")
	errCommit    = Error("Commit point does not exist in the storage system")
	errKey       = Error("Key does not exist in the storage system")
)
