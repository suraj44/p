package store

import (
	"container/heap"
)

// A Branch is something we manage in a length queue.
type Branch struct {
	leaf   *Node // The leaf of the branch; arbitrary.
	length int   // The length of the item in the queue.
	// The index is needed by update and is maintained by the heap.Interface methods.
	index int // The index of the item in the heap.
}

// A PriorityQueue implements heap.Interface and holds Branchs.
type PriorityQueue []*Branch

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, length so we use greater than here.
	return pq[i].length > pq[j].length
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Branch)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// update modifies the length and leaf of an Branch in the queue.
func (pq *PriorityQueue) update(item *Branch, leaf *Node, length int) {
	item.leaf = leaf
	item.length = length
	heap.Fix(pq, item.index)
}
