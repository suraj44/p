package store

import (
	"fmt"
	"math/rand"
	"runtime"
	"testing"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// Helper function for printing the current execution point
func printTestingInfo() {
	pc := make([]uintptr, 64)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	fmt.Printf("\n== Test Case ==\n\n%s:%d %s\n", frame.File, frame.Line, frame.Function)
}

// func TestStoreComplex(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(1)
// 	// Add nodes to the original dependency graph.
// 	//             root
// 	//      	/  |  |  \
// 	//         c   a  b   d
// 	//             | \ \  /|
// 	//             |  \ /  |
// 	//             |   / \ |
// 	//             d1   \  |
// 	//             |      d2
// 	//             f       |
// 	//			   | 	   g
// 	//             |      /
// 	//             |     /
// 	//             |    /
// 	//               e

// 	commitA, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	commitB, _ := s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("c", RandStringRunes(1), []uint64{0})
// 	commitD, _ := s.Set("d", "d", []uint64{0})
// 	s.Set("fall", RandStringRunes(1), []uint64{0})

// 	commitD1, _ := s.Set("d", "d1", []uint64{commitA, commitD})
// 	commitD2, _ := s.Set("d", "d2", []uint64{commitA, commitB, commitD})
// 	commitD3, _ := s.Set("d", "d3", []uint64{commitD})
// 	commitF, _ := s.Set("f", RandStringRunes(1), []uint64{commitD1})
// 	commitG, _ := s.Set("g", RandStringRunes(1), []uint64{commitD2})
// 	s.Set("g", RandStringRunes(1), []uint64{commitD2}) // should branch
// 	s.Set("x", RandStringRunes(1), []uint64{commitD3})
// 	commitE, _ := s.Set("e", RandStringRunes(1), []uint64{commitG, commitF}) // when we add this to the tree later, it should abort
// 	s.Set("e", RandStringRunes(1), []uint64{commitE})

// 	time.Sleep(time.Second / 10)
// 	// s.Consolidate()
// 	s.GenerateImage("img/Complex.png")

// 	if len(s.branchSet) != 3 {
// 		t.Errorf("expected 3 branches but got %d branches", len(s.branchSet))
// 	}

// 	if s.abortCount != 2 {
// 		t.Errorf("expected 2 aborts, but got %d", s.abortCount)
// 	}
// }

//// Single-layer graphs

func TestOneLayerGraphWithOneNode(t *testing.T) {
	printTestingInfo()
	s := NewStore(1)
	s.Set("a", RandStringRunes(1), 0, []uint64{0})

	s.Consolidate()
	s.GenerateImage("img/OneLayerGraphWithOneNode.png")

	// Expects one branch (i.e., one leaf node)
	if len(s.branchSet) != 1 {
		t.Errorf("Flatten: expected 1 branch but %d leaf nodes", len(s.branchSet))
	}

	if s.abortCount != 0 {
		t.Errorf("expected 0 aborts, but got %d", s.abortCount)
	}
}

// func TestOneLayerGraphWithSingleBranch(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(5)
// 	s.Set("a", RandStringRunes(1), []uint64{0})
// 	s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("c", RandStringRunes(1), []uint64{0})
// 	s.Set("d", RandStringRunes(1), []uint64{0})

// 	s.Consolidate()
// 	s.GenerateImage("img/OneLayerGraphWithSingleBranch.png")
// 	// Expects one branch (i.e., one leaf node)
// 	if len(s.branchSet) != 1 {
// 		t.Errorf("Flatten: expected 1 branch but %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected 0 aborts, but got %d", s.abortCount)
// 	}
// }

// func TestOneLayerGraphWithTwoSameNodes(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(5)

// 	s.Set("a", RandStringRunes(1), []uint64{0})
// 	s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("c", RandStringRunes(1), []uint64{0})
// 	s.Set("a", RandStringRunes(1), []uint64{0})

// 	s.Consolidate()
// 	s.GenerateImage("img/OneLayerGraphWithTwoSameNodes.png")

// 	// TODO: Expects multiple branches?
// }

// // *****************************
// // Multipe nodes access the same key at the first layer
// func TestOneLayerGraphWithMultiipleSameNodes(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(6)
// 	s.Set("a", RandStringRunes(1), []uint64{0})
// 	s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("c", RandStringRunes(1), []uint64{0})
// 	s.Set("a", RandStringRunes(1), []uint64{0})
// 	s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("c", RandStringRunes(1), []uint64{0})

// 	s.Consolidate()
// 	s.GenerateImage("img/OneLayerGraphWithMultiipleSameNodes.png")

// 	if len(s.branchSet) != 1 {
// 		t.Errorf("Flatten: expected 1 branch but %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// // //// Two-layer graphs

// // // B1 depends on A1
// // // B2 depends on A1
// // // Expect: two branches in the result
// func TestTwoLayersGraphWithTwoConflictNodes(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(3)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	s.Set("b", RandStringRunes(1), []uint64{cA1}) // cB1 depends on cA1
// 	s.Set("b", RandStringRunes(1), []uint64{cA1}) // cB2 depends on cA1

// 	s.Consolidate()
// 	s.GenerateImage("img/TwoLayersGraphWithTwoSameNodes.png")

// 	// Expects two branches (i.e., two leaf nodes)
// 	if len(s.branchSet) != 1 {
// 		t.Errorf("Flatten: expected 1 branch but %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// // // B1 depends on A1
// // // B2 depends on A1
// // // B3 depends on A1
// // // Expect: two branches in the result
// func TestTwoLayersGraphWithMultipleSameNodes(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(4)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	s.Set("b", RandStringRunes(1), []uint64{cA1}) // cB1 depends on cA1
// 	s.Set("b", RandStringRunes(1), []uint64{cA1}) // cB2 depends on cA1
// 	s.Set("b", RandStringRunes(1), []uint64{cA1}) // cB3 depends on cA1

// 	s.Consolidate()
// 	s.GenerateImage("img/TwoLayersGraphWithMultipleSameNodes.png")

// 	// Expects tree branches (i.e., three leaf nodes)
// 	if len(s.branchSet) != 1 {
// 		t.Errorf("Flatten: expected 1 branch but %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// // *******************************************
// // // A2 depends on A1
// // // B2 depends on B1
// // // Expect: single branch in the result
// func TestTwoLayersGraphWithNoCrossDep(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(5)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	cB1, _ := s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("a", RandStringRunes(1), []uint64{cA1}) // cA2 depends on cA1
// 	s.Set("b", RandStringRunes(1), []uint64{cB1}) // cB2 depends on cB1

// 	s.Consolidate()
// 	s.GenerateImage("img/TwoLayersGraphWithNoCrossDep.png")
// 	// Expects one branch (i.e., one leaf node)
// 	if len(s.branchSet) != 1 {
// 		t.Errorf("Flatten: expected 1 branch but %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// // ************
// // A2 depends on A1
// // B2 depends on A1, B1
// // Expect: single branch in the result
// func TestTwoLayersGraphWithSemiCrossDep(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(4)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	cB1, _ := s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("a", RandStringRunes(1), []uint64{cA1})      // cA2 depends on cA1
// 	s.Set("b", RandStringRunes(1), []uint64{cA1, cB1}) // cB2 depends on cA1, cB1

// 	s.Consolidate()
// 	s.GenerateImage("img/TwoLayersGraphWithSemiCrossDep.png")

// 	if len(s.branchSet) != 1 {
// 		t.Errorf("Flatten: expected 1 branch but %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// // A2 depends on B1
// // B2 depends on A1
// // Expect: two branches in the result
// func TestTwoLayersGraphWithCrossDep(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(5)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	cB1, _ := s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("a", RandStringRunes(1), []uint64{cB1}) // cA2 depends on cB1
// 	s.Set("b", RandStringRunes(1), []uint64{cA1}) // cB2 depends on cA1

// 	s.Consolidate()
// 	s.GenerateImage("img/TwoLayersGraphWithCrossDep.png")
// 	// Expects two branches (i.e., two leaf nodes)
// 	if len(s.branchSet) != 2 {
// 		t.Errorf("Flatten: expected 2 branches but got %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// // A2 depends on B1
// // B2 depends on A1, B1
// // Expect: two branches in the result
// func TestTwoLayersGraphWithSemiFullDep(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(5)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	cB1, _ := s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("a", RandStringRunes(1), []uint64{cB1})      // cA2 depends on cB1
// 	s.Set("b", RandStringRunes(1), []uint64{cA1, cB1}) // cB2 depends on cA1, cB1

// 	s.Consolidate()
// 	s.GenerateImage("img/TwoLayersGraphWithSemiFullDep.png")

// 	// Expects two branches (i.e., two leaf nodes)
// 	if len(s.branchSet) != 2 {
// 		t.Errorf("Flatten: expected 2 branches but got %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// // A2 depends on A1 and B1
// // B2 depends on A1 and B1
// // Expect: two branches in the result
// func TestTwoLayersGraphWithFullDep(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(4)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	cB1, _ := s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("a", RandStringRunes(1), []uint64{cA1, cB1}) // cA2 depends on cA1 and cB1
// 	s.Set("b", RandStringRunes(1), []uint64{cA1, cB1}) // cB2 depends on cA1 and cB1

// 	time.Sleep(time.Second / 10)
// 	s.Consolidate()
// 	s.GenerateImage("img/TwoLayersGraphWithFullDep.png")

// 	// Expects two branches (i.e., two leaf nodes)
// 	if len(s.branchSet) != 2 {
// 		t.Errorf("Flatten: expected 2 branches but got %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// // //// Three-layer graphs

// // // A2 depends on A1, B1
// // // B2 depends on A1
// // // A3 depends on A2, B2
// // // Expect: two branches for A2 and B2, and abort at A3
// func TestThreeLayersGraphWithSemiFullDepAndAbort(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(5)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	cB1, _ := s.Set("b", RandStringRunes(1), []uint64{0})
// 	cA2, _ := s.Set("a", RandStringRunes(1), []uint64{cA1, cB1}) // cA2 depends on cA1, cB1
// 	cB2, _ := s.Set("b", RandStringRunes(1), []uint64{cA1})      // cB2 depends on cA1
// 	s.Set("a", RandStringRunes(1), []uint64{cA2, cB2})           // cA3 depends on cA2, cB2

// 	s.Consolidate()
// 	s.GenerateImage("img/ThreeLayersGraphWithSemiFullDepAndAbort.png")

// 	if len(s.branchSet) != 2 {
// 		t.Errorf("Flatten: expected 2 branches but got %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 1 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// func TestMarking(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(5)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	s.Set("b", RandStringRunes(1), []uint64{0})
// 	cA2, _ := s.Set("a", RandStringRunes(1), []uint64{cA1}) // cA2 depends on cA1, cB1
// 	cB2, _ := s.Set("b", RandStringRunes(1), []uint64{cA1}) // cB2 depends on cA1
// 	s.Set("a", RandStringRunes(1), []uint64{cA2, cB2})      // cA3 depends on cA2, cB2

// 	s.Consolidate()
// 	s.GenerateImage("img/TestMarking.png")
// 	if len(s.branchSet) != 1 {
// 		t.Errorf("Flatten: expected 1 branch but got %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected no aborts but got %d", s.abortCount)
// 	}
// }

// func TestTwoLayerMarkedScenario(t *testing.T) {
// 	printTestingInfo()
// 	s := NewStore(5)
// 	cA1, _ := s.Set("a", RandStringRunes(1), []uint64{0})
// 	cB1, _ := s.Set("b", RandStringRunes(1), []uint64{0})
// 	s.Set("a", RandStringRunes(1), []uint64{cA1})      // cA2 depends on cA1, cB1
// 	s.Set("b", RandStringRunes(1), []uint64{cA1, cB1}) // cB2 depends on cA1
// 	// s.Set("a", RandStringRunes(1), []uint64{cA2, cB2})           // cA3 depends on cA2, cB2

// 	s.Consolidate()
// 	s.GenerateImage("img/TwoLayerMarkedScenario.png")

// 	if len(s.branchSet) != 1 {
// 		t.Errorf("Flatten: expected 2 branches but got %d leaf nodes", len(s.branchSet))
// 	}

// 	if s.abortCount != 0 {
// 		t.Errorf("expected 1 abort but got %d", s.abortCount)
// 	}
// }
