package store

import (
	"container/heap"
	"fmt"
	"math"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"

	// "github.com/sasha-s/go-deadlock"

	"github.com/suraj44/graph-kv/common"
	"github.com/suraj44/graph-kv/concurrent_map"
)

type policy int

const (
	dirtyRead      = iota
	consistentRead = iota
)

// Node struct is the definition for every element in the
// tree/graph
type Node struct {
	commit          uint64
	tid             uint64
	parents         []uint64 // this pointer will hold a useful value only after tree is flattened. Can leave as a pointer because at that point in time, there will be a node for this commit point on each shard so shouldn't be a problem.
	originalParents []uint64
	key             string
	value           string
	precommit       bool
	abort           bool
	flattened       bool
	depth           uint64
	shared          bool
	branchId        []uint64
	// children        []uint64
	// mark bool // used as a reference for visited nodes in epoch-based flattening
}
type NodeMetadata struct {
	Commit       uint64
	Key          string
	Tid          uint64
	Dependencies []uint64
}

type StorageSystem interface {
	// Insert a KV Pair into the storage system
	Set(key, value string, dependencies []uint64, policyId policy) (uint64, error)

	// Read the value at a given commit point
	GetbyCommit(commit uint64) (string, error)

	// Read the value of a key based on a policy
	GetbyKey(key string, policyId policy) (string, uint64, error)
}

// Store a type representing the storage system. It contains fields
// for the actual Data of the graph and secondary indices which allow one
// to reference the nodes in the graph by commit hash and their actual keys.
//
// This storage system is at its heart a multiversioned key-value store which can
// track dependencies between different operations. It can also detect conflicts in
// multiple write operations and create branches instead of aborting such conlficts.
type Store struct {
	// Data             []*Node
	Root *Node
	// Tree             *Tree
	FlattenLock sync.Mutex
	CommitIndex *concurrent_map.ConcurrentMap
	CommitLock  sync.Mutex
	KeyIndex    *concurrent_map.ConcurrentMap
	KeyLock     sync.Mutex
	TxnIndex    *concurrent_map.ConcurrentMap
	TxnLock     sync.Mutex
	// bufferedUpdates  map[uint64]*Node
	// keySnapshot      map[string][]uint64
	LeafSet          map[uint64]struct{}
	LeafLock         sync.Mutex
	TopologicalOrder []*Node
	TopologicalLock  sync.Mutex
	epochSize        uint64
	size             uint64
	flattenCount     uint64
	flattening       int32
	branchSet        PriorityQueue
	count            int
	insertCount      int
	epochChan        chan int
	routineFinish    bool
	branchIdSet      map[uint64]struct{}
	branchIdCounter  uint64
	abortCount       uint64
	// InitThreshold uint64
	// Threshold     uint64 // when size of system reaches threshold, we start bulldozing in background

}

func NewStore(epochSize uint64) *Store {
	// initialize store struct
	s := &Store{
		CommitIndex: concurrent_map.NewConcurrentMap(),
		KeyIndex:    concurrent_map.NewConcurrentMap(),
		// bufferedUpdates: make(map[uint64]*Node),
		// keySnapshot:     make(map[string][]uint64),
		LeafSet:   make(map[uint64]struct{}),
		TxnIndex:  concurrent_map.NewConcurrentMap(),
		epochSize: epochSize,
		size:      0,
	}

	// create a root node
	root := &Node{
		commit:   0,
		parents:  make([]uint64, 0),
		key:      "",
		value:    "",
		depth:    0,
		abort:    false,
		branchId: []uint64{0},
	}
	root.parents = append(root.parents, math.MaxUint64)

	// add it to the storage system
	s.Root = root
	// s.CommitIndex[0] = root
	s.CommitIndex.InsertUint64(0, root)
	// s.bufferedUpdates[0] = root
	s.KeyIndex.InsertIntoSlice("", 0)
	// s.KeyIndex[""] = append(s.KeyIndex[""], 0)
	// s.keySnapshot[""] = append(s.keySnapshot[""], 0)
	// s.TopologicalOrder = append(s.TopologicalOrder, root)
	s.flattenCount = 0

	s.insertCount = 0
	s.epochChan = make(chan int)
	s.routineFinish = false

	s.branchSet = make(PriorityQueue, 1)
	s.branchIdSet = make(map[uint64]struct{})
	s.branchIdCounter = 0
	s.abortCount = 0

	s.branchSet[0] = &Branch{
		leaf:   root,
		length: 1,
	}
	s.flattening = 0
	heap.Init(&s.branchSet)

	rand.Seed(time.Now().UTC().UnixNano())

	go s.FlattenRoutine()

	return s
}

// TODO: fix construct tree and then bring back flattenroutine.
func (s *Store) FlattenRoutine() {
	for {
		select {
		case n := <-s.epochChan:
			if n == 0 {
				s.insertCount++
				if s.insertCount >= int(s.epochSize) {
					snapShot := s.ReleaseBuffer(int(s.epochSize))
					s.insertCount -= len(snapShot)
					orderedSnap := s.GetTopologicalOrder(snapShot)
					s.ConstructTree(orderedSnap)
				}
			} else if n == 1 {
				fmt.Println("Consolidating...")
				snapShot := s.ReleaseBuffer(len(s.TopologicalOrder))
				s.insertCount -= len(snapShot)
				orderedSnap := s.GetTopologicalOrder(snapShot)
				s.ConstructTree(orderedSnap)
				s.routineFinish = true
				break
			}

		}
	}
}

func (s *Store) Consolidate() {
	s.epochChan <- 1
	for {
		if s.routineFinish {
			break
		}
	}

}

// can this generate math.MaxUint64? If so, that is a bug
func (s *Store) generateCommit() uint64 {
	// TODO: Check if generated commit is already used??
	return uint64(rand.Uint32())<<32 + uint64(rand.Uint32())
}

func (s *Store) Save(commit uint64, key string, tid uint64, dependencies []uint64) error {
	// log.Println("reached save()")
	// fmt.Println("started save in store")
	// s.CommitLock.Lock()
	// _, ok := s.CommitIndex[commit]
	// s.CommitLock.Unlock()
	// if ok {
	// 	fmt.Printf("commit point %d already exists. trying to insert node %s\n", commit, key)
	// 	// return common.ErrCommitExists
	// }

	node := &Node{
		parents:   dependencies,
		commit:    commit,
		tid:       tid,
		key:       key,
		value:     "",
		precommit: false,
		abort:     false,
		flattened: false,
		shared:    true,
	}

	s.CommitIndex.InsertUint64(node.commit, node)
	// s.CommitLock.Lock()
	// s.CommitIndex[node.commit] = node
	// // if atomic.CompareAndSwapInt32(&s.flattening, 0, 0) {
	// // 	s.bufferedUpdates[node.commit] = node
	// // }
	// s.CommitLock.Unlock()
	s.KeyIndex.InsertIntoSlice(key, node.commit)
	// s.KeyLock.Lock()
	// s.KeyIndex[key] = append(s.KeyIndex[key], node.commit)
	// // if atomic.CompareAndSwapInt32(&s.flattening, 0, 0) {
	// // 	s.keySnapshot[key] = append(s.keySnapshot[key], node.commit)
	// // }
	// s.KeyLock.Unlock()

	s.TxnIndex.InsertUint64IntoSlice(tid, node.commit)
	// fmt.Println("finsihed save in store")
	// log.Println("finished save()")

	return nil

}

func (s *Store) Set(key, value string, tid uint64, dependencies []uint64) (uint64, error) {
	// fmt.Printf("set\n")
	// try flattening the graph to a tree
	// TODO: If Txn is aborted, why bother writing????

	// Allocate memory for new node
	node := &Node{
		parents:   dependencies,
		commit:    s.generateCommit(),
		tid:       tid,
		key:       key,
		value:     value,
		precommit: false,
		abort:     false,
		flattened: false,
		shared:    false,
	}
	// fmt.Println(node.key)

	// Add node to data structures
	// s.Data = append(s.Data, node)
	// TODO: Find replacement for topological order!
	// s.TopologicalLock.Lock()
	// s.TopologicalOrder = append(s.TopologicalOrder, node)
	// s.TopologicalLock.Unlock()

	s.CommitIndex.InsertUint64(node.commit, node)
	// s.CommitLock.Lock()
	// s.CommitIndex[node.commit] = node
	// // if atomic.CompareAndSwapInt32(&s.flattening, 0, 0) {
	// // 	s.bufferedUpdates[node.commit] = node
	// // }
	// s.CommitLock.Unlock()
	s.KeyIndex.InsertIntoSlice(key, node.commit)
	// s.KeyLock.Lock()
	// s.KeyIndex[key] = append(s.KeyIndex[key], node.commit)
	// // if atomic.CompareAndSwapInt32(&s.flattening, 0, 0) {
	// // 	s.keySnapshot[key] = append(s.keySnapshot[key], node.commit)
	// // }
	// s.KeyLock.Unlock()

	s.TxnIndex.InsertUint64IntoSlice(tid, node.commit)

	go func() {
		s.epochChan <- 0
	}()
	// s.TxnLock.Lock()
	// s.TxnIndex[tid] = append(s.TxnIndex[tid], node.commit)
	// s.TxnLock.Unlock()
	// s.size++

	// s.LeafLock.Lock()

	// for _, dep := range dependencies {
	// 	if _, ok := s.LeafSet[dep]; ok {
	// 		delete(s.LeafSet, dep)
	// 	}
	// }
	// s.LeafSet[node.commit] = struct{}{}

	// s.LeafLock.Unlock()
	// s.epochChan <- 0
	go func() {
		s.epochChan <- 0
	}()
	// s.ConstructTree(false)
	// go func() {
	// s.ConstructTree(false)
	// }()

	return node.commit, nil
}

func (s *Store) ReleaseBuffer(n int) []NodeMetadata {
	s.TopologicalLock.Lock()
	count := 0
	var ret []NodeMetadata
	fmt.Printf("size of topo order is %d. n is %d\n", len(s.TopologicalOrder), n)
	for {
		if len(s.TopologicalOrder) == 0 || count >= n {
			break
		}
		node := s.TopologicalOrder[0]
		s.TopologicalOrder = s.TopologicalOrder[1:]

		metadata := NodeMetadata{
			Commit:       node.commit,
			Key:          node.key,
			Tid:          node.tid,
			Dependencies: node.parents,
		}
		ret = append(ret, metadata)
		node.shared = true

	}
	// fmt.Printf("releasing list of  %d\n", len(ret))
	s.TopologicalLock.Unlock()
	return ret
}

func NewNodeMetadata(n *Node) *NodeMetadata {
	return &NodeMetadata{
		Key:          n.key,
		Commit:       n.commit,
		Tid:          n.tid,
		Dependencies: n.parents,
	}
}

func (s *Store) FindMissingParents(buffer []*NodeMetadata) []*NodeMetadata {
	var ret []*NodeMetadata
	for _, node := range buffer {
		for _, dep := range node.Dependencies {
			node := s.GetNode(dep)
			/*
			 * If this node was originally written to this shard
			 * and has not been shared yet, add it to the output
			 */
			if node != nil && !node.shared {
				ret = append(ret, NewNodeMetadata(node))
			}
		}

	}
	return ret
}

func (s *Store) GetTopologicalOrder(order []NodeMetadata) []NodeMetadata {

	var queue []NodeMetadata
	deps := make(map[uint64]struct{})
	commitPointerMap := make(map[uint64]NodeMetadata)
	// create a set of all the dependencies
	for _, node := range order {
		for _, parent := range node.Dependencies {
			if _, ok := deps[parent]; !ok {
				deps[parent] = struct{}{}
			}
			commitPointerMap[node.Commit] = node
		}
	}

	// record all nodes that do not feature in the set.
	// they are leaves
	var final []NodeMetadata
	visited := make(map[uint64]struct{})
	for _, node := range order {
		if _, ok := deps[node.Commit]; !ok {
			// if node == nil {
			// 	fmt.Println("NIL NODE")
			// }
			queue = append(queue, node)
			final = append(final, node)
			visited[node.Commit] = struct{}{}
		}
		if len(node.Dependencies) > 0 {
		}
	}

	for {
		if len(queue) == 0 {
			break
		}

		currNode := queue[0]

		queue = queue[1:]
		// fmt.Println(currNode)

		// fmt.Println(currNode)

		for _, parent := range currNode.Dependencies {
			if _, ok := visited[parent]; !ok {
				parentNode := commitPointerMap[parent]
				queue = append(queue, parentNode)
				visited[parent] = struct{}{}
				final = append(final, parentNode)
			}
		}
	}

	return final
}

func checkSubset(first, second []uint64) bool {
	if len(first) == 0 {
		return true
	}
	if len(second) >= len(first) {
		ind := len(first) - 1
		// fmt.Printf("%v %v\n", first, second)
		if first[ind] == second[ind] {
			return true
		}
	} else {
		ind := len(second) - 1
		if first[ind] == second[ind] {
			return true
		}
	}

	return false
}

func (s *Store) abortTxn(tid uint64) (abortedNodes []uint64) {
	txnNodes, _ := s.TxnIndex.ReadUint64Slice(tid)
	for _, commit := range txnNodes {
		node := s.GetNode(commit)
		if node.abort {
			continue
		}
		node.abort = true
		s.abortCount++
		abortedNodes = append(abortedNodes, node.commit)
	}

	return abortedNodes
}

func (s *Store) ConstructTree(list []NodeMetadata) bool {
	if !atomic.CompareAndSwapInt32(&s.flattening, 0, 1) {
		return false
	}
	// fmt.Printf("leafset %d\n", len(s.LeafSet))

	// rootBranch := &Branch{
	// 	leaf:   s.GetNode(0),
	// 	length: 1,
	// }
	// heap.Push(&s.branchSet, rootBranch)

	var count uint64
	count = 0
	updateSet := make([]uint64, s.epochSize)
	bufferedUpdates := make(map[uint64]*Node, s.epochSize)
	keySnapshot := make(map[string][]uint64, s.epochSize)
	s.CommitLock.Lock()
	s.KeyLock.Lock()
	// for key, value := range s.CommitIndex {
	// 	bufferedUpdates[key] = value
	// }
	// // bufferedUpdates = s.CommitIndex
	// for key, value := range s.KeyIndex {
	// 	keySnapshot[key] = value
	// }
	s.KeyLock.Unlock()
	s.CommitLock.Unlock()

	// bufferedUpdates[0] = s.GetNode(0)

	for {
		if len(list) <= 0 {
			break
		}
		// if len(list)%1000 == 0 {
		// 	fmt.Println("Aborts:", s.abortCount)
		// 	fmt.Println("Branches:", len(s.branchSet))

		// }

		// Pop Node from queue
		head := list[0]
		list = list[1:]

		currNode := s.GetNode(head.Commit)

		// fmt.Println("currnode", currNode.key)
		// fmt.Println(len(s.branchSet))
		// don't try flattening the root node
		if currNode.commit == 0 {
			continue
		}
		bufferedUpdates[currNode.commit] = currNode
		keySnapshot[currNode.key] = append(keySnapshot[currNode.key], currNode.commit)
		// fmt.Printf("check\n")

		dependencySet := make(map[uint64]struct{})
		// fmt.Println(currNode.commit)
		for _, parent := range currNode.parents {
			dependencySet[parent] = struct{}{}
		}

		blindWrite := true
		// var oldVersionCommit uint64 = 0

		depKeyMap := make(map[string]uint64)

		for _, p := range currNode.parents {
			pVal, _ := s.CommitIndex.ReadUint64(p)
			parent := pVal.(*Node)
			if parent.key == currNode.key {
				blindWrite = false
				// oldVersionCommit = parent
			}
			depKeyMap[parent.key] = p
		}

		// find branch that contains all dependencies of this node
		found := false
		conflict := false
		var branchPoint *Node = nil
		conflictHeight := 0
		mark := false
		var markPoint *Node = nil
		// var firstParent uint64 = 0

		tmpBranch := make([]*Branch, 0)
		var branch *Branch

		// set the branch Ids of all the parents into an ordered list - why are we using a map insteed?? TODO:
		parentIds := [][]uint64{}

		// if any dependency node or other member of the txn has aborted, abort currNode as well
		abort := false
		for _, dirtyParent := range currNode.parents {
			parent, ok := bufferedUpdates[dirtyParent]
			if !ok || parent.abort { // segfaults here uNum=100. Maybe parent gets garbage collected?
				abort = true

				break
			}
			parentIds = append(parentIds, parent.branchId)
		}
		s.TxnLock.Lock()
		txnNodes, _ := s.TxnIndex.ReadUint64Slice(currNode.tid)
		for _, commit := range txnNodes {
			node := s.GetNode(commit)
			// do not include subsequent writes in this check (when looking or branches to add currNode to)
			if commit == currNode.commit {
				break
			}
			if node.abort {
				abort = true
				break
			}
			parentIds = append(parentIds, node.branchId)
		}
		s.TxnLock.Unlock()
		if abort {
			abortedNodes := s.abortTxn(currNode.tid)
			for _, commit := range abortedNodes {
				n := s.GetNode(commit)
				n.abort = true
			}
			updateSet = append(updateSet, abortedNodes...)
			continue
		}
		if currNode.key == "g" {
			println("key:", currNode.key, "parent IDs:", parentIds)
		}

		// traverse this list and make sure the subset property is maintained

		// if it is, all parents are on the same branch. the longest branchId is what we need to check
		// with the leaves of branchset to determine which branch is correct

		// if the property is not maintained, abort.

		for {
			// fmt.Println("traversing...")
			if len(s.branchSet) == 0 {
				break
			}
			branch = heap.Pop(&s.branchSet).(*Branch)

			found = true
			for _, id := range parentIds {
				// fmt.Printf("%v %v\n", id, parentIds)
				// fmt.Println(branch.leaf)
				if !checkSubset(id, branch.leaf.branchId) {
					found = false
					break
				}
			}

			tmpBranch = append(tmpBranch, branch)
			if found {
				break
			}

		}

		for _, poppedBranch := range tmpBranch {
			heap.Push(&s.branchSet, poppedBranch)
		}

		// if there is no branch that contains all dependencies and other preceeding txns, abort.
		if !found {
			abortedNodes := s.abortTxn(currNode.tid)
			for _, commit := range abortedNodes {
				bufferedUpdates[commit].abort = true
			}
			updateSet = append(updateSet, abortedNodes...)
			continue
		}
		// if firstParent != s.Root.commit {
		// 	fmt.Println("first parent", s.GetNode(firstParent).key, s.GetNode(firstParent).value, s.GetNode(s.GetNode(firstParent).parents[0]).key)
		// }
		var conflictNode *Node
		var firstParent *Node

		maxDepth := -1
		// find the firstParent

		// TODO: Do we need to consider other nodes within the transaction here?
		for _, dirtyParent := range currNode.parents {
			parent := bufferedUpdates[dirtyParent]
			// fmt.Printf("%s %d\n", parent.key, parent.depth)
			if int(parent.depth) > maxDepth {
				// fmt.Printf("%s %d\n", parent.key, parent.depth)
				firstParent = parent
				maxDepth = int(firstParent.depth)
			}
		}

		var conflictDepth uint64 = math.MaxInt32

		// fmt.Printf("currNode: %s %s firstParent: %s \n", currNode.key, currNode.value, firstParent.key)
		// fmt.Printf("%d\n", len(currNode.parents))
		if !blindWrite {
			// fmt.Printf("firstParent %s %d\n", firstParent.key, firstParent.depth)
			for _, commit := range keySnapshot[currNode.key] {
				candidate, ok := bufferedUpdates[commit]
				if !ok {
					continue
				}
				// fmt.Printf("conflict candidate %s %d %d\n", candidate.key, candidate.depth, conflictDepth)
				if candidate.depth < conflictDepth {
					if checkSubset(candidate.branchId, branch.leaf.branchId) && candidate.depth > firstParent.depth {

						conflictDepth = candidate.depth
						conflict = true
						branchPoint = bufferedUpdates[candidate.parents[0]]
						// fmt.Println(candidate.key, currNode.key, candidate.flattened, branchPoint.flattened, branchPoint)
						conflictNode = candidate
						conflictHeight = int(branch.leaf.depth) - int(candidate.depth)
					}
				}
			}
		}

		for _, p := range currNode.parents {
			parent := bufferedUpdates[p]
			for _, candidateCommit := range keySnapshot[parent.key] {
				candidate, ok := bufferedUpdates[candidateCommit]
				if !ok {
					continue
				}
				// fmt.Printf("mark candidate %s %d %d\n", candidate.key, candidate.depth, conflictDepth)
				if candidate.depth < uint64(conflictDepth) {
					if candidate.commit != parent.commit && checkSubset(candidate.branchId, branch.leaf.branchId) && candidate.depth > firstParent.depth {
						canMark := true
						// fmt.Printf("found something\n")
						for _, ogParent := range candidate.originalParents {
							og := bufferedUpdates[ogParent]
							if og.key == currNode.key {
								canMark = false
								break
							}
						}

						if canMark {
							mark = true
							markPoint = candidate
							conflictDepth = candidate.depth
						} else {
							conflictDepth = candidate.depth
							conflict = true
							branchPoint = bufferedUpdates[candidate.parents[0]]
							conflictNode = candidate
							conflictHeight = int(branch.leaf.depth) - int(candidate.depth)
						}
					}

				}
			}
		}
		// TODO: check which comes first, conflict or marking and prefer that..
		count++
		branchId := []uint64{}
		updateSet = append(updateSet, currNode.commit)
		if conflict {
			// fmt.Println("conflict")
			// fmt.Printf("c")
			// fmt.Printf("%v %v \n", conflictNode.branchId, branchPoint.branchId)
			branchId = append(branchId, branchPoint.branchId...)
			if conflictNode.branchId[len(conflictNode.branchId)-1] == branchPoint.branchId[len(branchPoint.branchId)-1] {
				//if checkSubset(conflictNode.branchId, branchPoint.branchId) {
				// fmt.Printf("before conf: %v\n", conflictNode.branchId)
				atomic.AddUint64(&s.branchIdCounter, 1)
				conflictNode.branchId = append(conflictNode.branchId, s.branchIdCounter) // [s.branchIdCounter] = struct{}{}
				// fmt.Printf("after conf conf: %v\n", conflictNode.branchId)
			}
			// if reflect.DeepEqual(conflictNode.branchId, branchPoint.branchId) {
			// 	fmt.Printf("before conf: %v\n", conflictNode.branchId)
			// 	atomic.AddUint64(&s.branchIdCounter, 1)
			// 	conflictNode.branchId[s.branchIdCounter] = struct{}{}
			// 	fmt.Printf("after conf conf: %v\n", conflictNode.branchId)
			// }
			atomic.AddUint64(&s.branchIdCounter, 1)
			branchId = append(branchId, s.branchIdCounter) // [s.branchIdCounter] = struct{}{}
			node := &Node{
				parents:         []uint64{branchPoint.commit},
				originalParents: currNode.parents,
				commit:          currNode.commit,
				tid:             currNode.tid,
				key:             currNode.key,
				value:           currNode.value,
				branchId:        branchId,
				depth:           branchPoint.depth + 1,
				flattened:       true,
			}
			// s.CommitLock.Lock()
			// s.CommitIndex[currNode.commit] = node
			// s.CommitLock.Unlock()
			bufferedUpdates[currNode.commit] = node
			newBranch := &Branch{
				leaf:   node,
				length: branch.length - conflictHeight,
			}
			heap.Push(&s.branchSet, newBranch)
			//fmt.Printf("len of parents: %d\n", len(node.parents))
			// s.branchSet.update(branch, node, branch.length-conflictHeight)
		} else if mark {
			// fmt.Printf("m")
			// fmt.Println("marked")
			branchId = append(branchId, markPoint.branchId...)
			node := &Node{
				parents:         []uint64{markPoint.parents[0]},
				originalParents: currNode.parents,
				commit:          currNode.commit,
				tid:             currNode.tid,
				key:             currNode.key,
				value:           currNode.value,
				branchId:        branchId,
				depth:           markPoint.depth,
				flattened:       true,
			}
			markPoint.parents[0] = node.commit
			// s.CommitLock.Lock()
			// s.CommitIndex[currNode.commit] = node
			// s.CommitLock.Unlock()
			// fmt.Printf("len of parents: %d\n", len(node.parents))
			bufferedUpdates[currNode.commit] = node

			s.branchSet.update(branch, branch.leaf, branch.length+1)
		} else {
			// fmt.Printf("f")
			// fmt.Println("flattened")
			if branch.leaf.commit == 0 {
				atomic.AddUint64(&s.branchIdCounter, 1)
				branchId = append(branch.leaf.branchId, s.branchIdCounter) //[s.branchIdCounter] = struct{}{}
			} else {
				branchId = append(branchId, branch.leaf.branchId...)
			}
			node := &Node{
				parents:         []uint64{branch.leaf.commit},
				originalParents: currNode.parents,
				commit:          currNode.commit,
				tid:             currNode.tid,
				key:             currNode.key,
				value:           currNode.value,
				branchId:        branchId,
				depth:           branch.leaf.depth + 1,
				flattened:       true,
			}
			// s.CommitLock.Lock()
			// s.CommitIndex[currNode.commit] = node
			// s.CommitLock.Unlock()
			// fmt.Printf("len of parents: %d\n", len(node.parents))
			bufferedUpdates[currNode.commit] = node
			s.branchSet.update(branch, node, branch.length+1)
		}

	}

	// s.CommitLock.Lock()
	for _, commit := range updateSet {
		s.CommitIndex.InsertUint64(commit, bufferedUpdates[commit])
		// s.CommitIndex[commit] = bufferedUpdates[commit]
	}
	// // for commit, newPointer := range bufferedUpdates {
	// // 	s.CommitIndex[commit] = newPointer
	// // }
	// s.CommitLock.Unlock()

	tmpBranch := make([]*Branch, 0)
	s.LeafLock.Lock()
	for {
		if len(s.branchSet) <= 0 {
			break
		}
		branch := heap.Pop(&s.branchSet).(*Branch)
		s.LeafSet[branch.leaf.commit] = struct{}{}
		tmpBranch = append(tmpBranch, branch)
	}
	s.LeafLock.Unlock()
	for _, poppedBranch := range tmpBranch {
		heap.Push(&s.branchSet, poppedBranch)
	}

	s.flattenCount++
	// s.GenerateImage("img/tmp_graph_" + strconv.Itoa(int(s.flattenCount)) + ".png")

	fmt.Println("Aborts:", s.abortCount)
	fmt.Println("Branches:", len(s.branchSet))
	// s.Print()
	atomic.CompareAndSwapInt32(&s.flattening, 1, 0)

	return true

}

func (s *Store) GetbyCommit(commit uint64) (string, error) {
	// fmt.Printf("getbycommit\n")
	// fmt.Println("commit ", commit)
	val, ok := s.CommitIndex.ReadUint64(commit)
	if !ok { // s.CommitIndex[commit]; !ok {
		return "", common.ErrCommit
	}

	return val.(*Node).value, nil
}

func (s *Store) GetbyKey(key string, policyId int) (string, uint64, error) {
	// fmt.Printf("getbykey\n")

	// check for existence of key in storage system
	list, ok := s.KeyIndex.ReadSlice(key) //s.KeyIndex[key]
	if !ok {
		return "", math.MaxUint64, common.ErrKey
	}
	switch policyId {
	case dirtyRead:
		val, _ := s.CommitIndex.ReadUint64(list[len(list)-1]) // s.CommitIndex[list[len(list)-1]]
		node := val.(*Node)
		return node.value, node.commit, nil
	case consistentRead:

		for i := len(list) - 1; i >= 0; i-- {
			val, _ := s.CommitIndex.ReadUint64(list[i]) // s.CommitIndex[list[len(list)-1]]
			node := val.(*Node)
			if node.flattened {
				return node.value, node.commit, nil
			}
		}
		// if we can't find a consistent value, return a dirty value instead
		val, _ := s.CommitIndex.ReadUint64(list[len(list)-1]) // s.CommitIndex[list[len(list)-1]]
		node := val.(*Node)
		return node.value, node.commit, errDirtyRead
	}

	//return just to satisfy compiler
	return "", math.MaxUint64, nil
}

type StoreTxn struct {
	store *Store
	tId   int
}

func (s *Store) NewStoreTxn() *StoreTxn {
	s.count++
	tId := s.count

	t := &StoreTxn{
		store: s,
		tId:   tId,
	}

	return t
}

func (s *StoreTxn) Read(key string) (string, uint64, string) {
	value, commit, err := s.store.GetbyKey(key, dirtyRead)
	return value, commit, err.Error()
}

func (s *StoreTxn) Write(key, val string, dependencies []uint64) (uint64, bool) {
	commit, _ := s.store.Set(key, val, uint64(s.tId), dependencies)
	return commit, true
}

func (s *StoreTxn) Commit() bool {
	return true

}

func (s *StoreTxn) Abort() {

}

// // func (s *Store) Get(string key, uint64 commit) ([]Node, error) {
// // 	if _, ok := s.KeyIndex[]; !ok {
// // 		return nil, errors.New("")
// // 	}

// // 	// unknown commit, return all nodes corresponding to this key
// // 	if commit == math.MaxUint64 {

// // 	}
// // }

func (s *Store) Print() {
	s.CommitLock.Lock()
	defer s.CommitLock.Unlock()
	fmt.Println("REACHED PRINT")
	// for _, node := range s.CommitIndex {
	// 	fmt.Printf("Commit Hash: %20d BranchID: %v Key: %10s Value: %10s Abort: %v Parent: %v\n", node.commit, node.branchId, node.key, node.value, node.abort, node.parents)
	// }
}

// // GetTopologicalOrder performs a BFS on the dependency graph
// // Append each note you visit to an output list
// // The output list will contain the nodes of the graph
// // in a topological order.
// func (s *Store) GetTopologicalOrder(count uint64) ([]*Node, error) {
// 	t := count
// 	root, err := s.GetbyCommit(0)
// 	if err != nil {
// 		return []*Node{}, errors.New("Error in calling get() on root node")
// 	}
// 	outputList := make([]*Node, 0)
// 	queue := make([]*Node, 0)
// 	queue = append(queue, root)
// 	existenceMap := make(map[uint64]struct{})
// 	for {
// 		if len(queue) == 0 {
// 			break
// 		}
// 		currNode := queue[0]
// 		queue = queue[1:]
// 		// skip processing this node if it has already been visited
// 		if count == 0 {
// 			break
// 		}
// 		for _, child := range currNode.children {
// 			childNode, getErr := s.GetbyCommit(child)

// 			// fmt.Println("child key: ", childNode.key)
// 			if getErr != nil {
// 				return []*Node{}, errors.New("Error in calling get() on a child node")
// 			}
// 			// append only if we have not come across this node before
// 			if _, ok := existenceMap[childNode.commit]; !ok && !childNode.mark {
// 				currNode.mark = true // don't process this node again
// 				count--
// 				outputList = append(outputList, childNode)
// 				queue = append(queue, childNode)
// 				existenceMap[childNode.commit] = struct{}{}
// 				if count == 0 {
// 					break
// 				}
// 			}

// 		}

// 	}
// 	fmt.Printf("getTopologicalSort: count: %d len(outputList): %d\n", t, len(outputList))
// 	return outputList, nil
// }

// // GetSeparateTopologicalOrder works the same way as GetTopologicalSort but it will not consider if
// // nodes are marked. It will topologically sort ALL nodes in the graph. This can be used in test cases
// // to verify that the generated serialized tree is correct.

// func (s *Store) GetSeparateTopologicalOrder() ([]*Node, error) {
// 	root, err := s.GetbyCommit(0)
// 	if err != nil {
// 		return []*Node{}, errors.New("Error in calling get() on root node")
// 	}
// 	outputList := make([]*Node, 0)
// 	queue := make([]*Node, 0)
// 	queue = append(queue, root)
// 	existenceMap := make(map[uint64]struct{})
// 	for {
// 		if len(queue) == 0 {
// 			break
// 		}
// 		currNode := queue[0]
// 		queue = queue[1:]
// 		for _, child := range currNode.children {
// 			childNode, getErr := s.GetbyCommit(child)

// 			// fmt.Println("child key: ", childNode.key)
// 			if getErr != nil {
// 				return []*Node{}, errors.New("Error in calling get() on a child node")
// 			}
// 			// append only if we have not come across this node before
// 			if _, ok := existenceMap[childNode.commit]; !ok {
// 				outputList = append(outputList, childNode)
// 				queue = append(queue, childNode)
// 				existenceMap[childNode.commit] = struct{}{}

// 			}

// 		}

// 	}
// 	return outputList, nil
// }

// func (s *Store) Flatten() {

// }

func (s *Store) GetNode(commit uint64) *Node {
	val, _ := s.CommitIndex.ReadUint64(commit)
	return val.(*Node)

}

// func (s *Store) GenerateImage(imageName string) error {
// 	// root, _ := s.GetbyCommit(0)
// 	// if err != nil {
// 	// 	return []*Node{}, errors.New("Error in calling get() on root node")
// 	// }
// 	fmt.Printf("Called generate image\n")
// 	queue := make([]*Node, 0)

// 	// add sourcen nodes to the queue
// 	s.LeafLock.Lock()
// 	for leaf := range s.LeafSet {
// 		currNode := s.GetNode(leaf)
// 		queue = append(queue, currNode)
// 	}
// 	s.LeafLock.Unlock()

// 	existenceMap := make(map[uint64]*cgraph.Node)

// 	g := graphviz.New()
// 	graph, err := g.Graph()

// 	existenceMap[0], err = graph.CreateNode("root" + ":" + "")

// 	if err != nil {
// 		return err
// 	}
// 	defer func() {
// 		if err := graph.Close(); err != nil {
// 			log.Fatal(err)
// 		}
// 		g.Close()
// 	}()

// 	root := s.GetNode(0)
// 	for {
// 		if len(queue) == 0 {
// 			break
// 		}
// 		currNode := queue[0]
// 		// ancestor, err := graph.CreateNode(currNode.Key + ":" + strconv.Itoa(int(currNode.Depth)))
// 		if err != nil {
// 			return err
// 		}

// 		queue = queue[1:]
// 		if currNode == root {
// 			continue
// 		}

// 		if len(currNode.branchId) == 0 {
// 			continue
// 		}

// 		if _, ok := existenceMap[currNode.commit]; !ok && !currNode.abort {
// 			var abort string
// 			if currNode.abort {
// 				abort = " -- aborted!"
// 			} else {
// 				abort = ""
// 			}
// 			existenceMap[currNode.commit], err = graph.CreateNode(currNode.key + ": " + currNode.value + abort + fmt.Sprintf("\n %v", currNode.branchId))
// 			// fmt.Printf("%s %s %v\n", currNode.key, currNode.value, currNode.branchId)
// 		}

// 		for _, parent := range currNode.parents {
// 			parentNode := s.GetNode(parent)

// 			// fmt.Println(parentNode.key)

// 			// // fmt.Println("child key: ", childNode.key)
// 			// if getErr != nil {
// 			// 	return errors.New("Error in calling get() on a child node")
// 			// }
// 			// append only if we have not come across this node before
// 			//if _, ok := existenceMap[childNode.commit]; !ok {
// 			// descendant, err := graph.CreateNode(childNode.Key + ":" + strconv.Itoa(int(childNode.Depth)))
// 			// if err != nil {+
// 			// 	return err
// 			// }

// 			queue = append(queue, parentNode)
// 			if _, ok := existenceMap[parentNode.commit]; !ok {
// 				var abort string
// 				if parentNode.abort {
// 					abort = " -- aborted!"
// 				} else {
// 					abort = ""
// 				}
// 				if !parentNode.abort {
// 					existenceMap[parentNode.commit], err = graph.CreateNode(parentNode.key + ": " + parentNode.value + abort + fmt.Sprintf("\n %v", parentNode.branchId))
// 					// fmt.Printf("%s %s %v\n", parentNode.key, parentNode.value, parentNode.branchId)
// 				}

// 			}

// 			if !currNode.abort && !parentNode.abort {
// 				_, err = graph.CreateEdge("e", existenceMap[parentNode.commit], existenceMap[currNode.commit])
// 			}

// 			//			}

// 		}

// 	}

// 	// 3. write to file directly
// 	if err := g.RenderFilename(graph, graphviz.PNG, imageName); err != nil {
// 		log.Fatal(err)
// 	}
// 	return nil
// }
