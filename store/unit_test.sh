PATH_TO_TEST="`pwd`"

## Single-layer graphs
go test -v -run TestOneLayerGraphWithTwoConflictNodes $PATH_TO_TEST
go test -v -run TestOneLayerGraphWithMultiipleConflictNodes $PATH_TO_TEST

## Two-layer graphs
#go test -v -run TestTwoLayersGraphWithTwoConflictNodes $PATh_TO_TEST
#go test -v -run TestTwoLayersGraphWithMultipleConflictNodes $PATh_TO_TEST
#go test -v -run TestTwoLayersGraphWithNoCrossDep $PATH_TO_TEST
#go test -v -run TestTwoLayersGraphWithSemiCrossDep $PATH_TO_TEST
go test -v -run TestTwoLayersGraphWithCrossDep $PATH_TO_TEST
go test -v -run TestTwoLayersGraphWithSemiFullDep $PATH_TO_TEST
go test -v -run TestTwoLayersGraphWithFullDep $PATH_TO_TEST

## Three-layer graphs
go test -v -run TestThreeLayersGraphWithSemiFullDepAndAbort $PATH_TO_TEST

