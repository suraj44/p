# Graph KV Store

This repository contains the code for Graph-KV: A Dependency Tracking Transactional System.

Directory Structure:

- `client/` - Client library with transactional interface
- `db/` - proto and go files that degine gRPC interface
- `common/` - currently only defines errors
- `indexer/` - consistent hashing logic
- `rpc-server/` - deprecated single-server implementation
- `server/` - current sharded implementation of system
- `shard/` - defines interface for key-partitioning
- `shard-store/` - deprecated
- `store/` - actual implementation of dependency tree based storage system
- `workload/` - Contains workloads implemented on top of the system and shell scripts to automate experiments

The system has a key value interface:

```go
Set(key string, value string, tid uint64, dependencies []uint64, save bool) (uint64, error)

GetbyKey(key string, policyId int) (string, uint64, error)
```

Each `Set()` operation returns a commit hash which uniquely represents that node that is stored in the dependency graph of the system. Even on `GetbyKey()` calls, the commit point of the node is returned along with the value. These commit points can be used as explicit dependencies in future `Set()` calls which would form the edges in the dependency graph. The system periodically 'flattens' this graph to serially the nodes in the graph. The implementation of the system can be found in `/server` and `/store`.

The system is sharded using consistent hashing if multiple nodes are to be used. In the sharded scenario, there is a state machine that is followed to create a global snapshot of the system so that each shard can independently serially order the writes in the system. 

Storing data in the form of a dependency graph allows us to employ a 'branch-on-conflict' concurrency control policy. Rather having to abort and retry writes in the case of a write-write conflict, a new branch can be created. This in turn allows us to support asynchronous writes to the system which will be useful in the context of serverless computing which is the intended usecase of the system. 

There is a client library implemented in `/client` to connect to the system. It implements a transactional interface. New transactions can be created by calling `c.NewClientTxn()`. An example is shown below: 

```go
func TestComplex(t *testing.T) {
	c, _ := NewClient([]string{"10.0.1.4:5000", "10.0.1.5:5000"}) // IP addresses of all shards

	value := []byte("5")
	txn := c.NewClientTxn() // creates a new txn ID
	commitA, _ := txn.Write("a", string(value), []uint64{0})
	txn.Commit()
	txn = c.NewClientTxn()
	commitB, _ := txn.Write("b", string(value), []uint64{commitA}) // explicitly specify previous commit as dependency
	txn.Commit()
	time.Sleep(2 * time.Second)
	c.Print()
}
```
The client determines which shard a write should be sent to on its own to prevent an extra roundtrip that would be required to handle this on the server-side.