package main

import (
	"fmt"
	"sync"

	"github.com/suraj44/graph-kv/workload"
)

//// Key-value DB Driver
type KvDriver struct {
	db *KvDB
}

func NewKvDriver(db *KvDB) workload.Driver {
	d := &KvDriver{
		db: db,
	}
	return d
}

func (d *KvDriver) BeginTxn() workload.Txn {
	return d.db.NewKvTxn()
}

//// Key-value DB
type KvDB struct {
	kv     map[string]string
	kvLock sync.RWMutex
	count  int
	lock   sync.Mutex
}

func NewKvDB() *KvDB {
	db := &KvDB{
		kv:    make(map[string]string),
		count: 0,
	}
	return db
}

func (db *KvDB) NewKvTxn() *KvTxn {
	db.lock.Lock()
	db.count++
	tId := db.count
	db.lock.Unlock()

	t := &KvTxn{
		db:  db,
		tId: tId,
	}

	return t
}

func (db *KvDB) printKv() { // Helper function
	db.kvLock.Lock()
	defer db.kvLock.Unlock()
	fmt.Println("=== DB Snapshot Start===")
	for k, v := range db.kv {
		fmt.Println(k, v)
	}
	fmt.Println("=== DB Snapshot End===")
}

func (db *KvDB) clean() { // Helper function
	db.kvLock.Lock()
	defer db.kvLock.Unlock()

	db.kv = make(map[string]string)
}

//// Key-value txn handle
type KvTxn struct {
	db  *KvDB
	tId int
}

func (t *KvTxn) Read(key string) (string, uint64) {
	t.db.kvLock.RLock()
	defer t.db.kvLock.RUnlock()
	return t.db.kv[key], 0
}
func (t *KvTxn) Write(key, val string, dependencies []uint64) (uint64, bool) {
	t.db.kvLock.Lock()
	defer t.db.kvLock.Unlock()
	t.db.kv[key] = val
	return 0, true
}

func (t *KvTxn) Commit() bool {
	return true
}

func (t *KvTxn) Abort() {
}
