package store

import (
	"errors"
	"fmt"
	"math"
	"math/rand"
	"sync"
)

// Node struct is the definition for every element in the
// tree/graph
type Node struct {
	commit   uint64
	parents  []uint64
	key      string
	value    []byte
	children []uint64
}

// Store a type representing the storage system. It contains fields
// for the actual Data of the graph and secondary indices which allow one
// to reference the nodes in the graph by commit hash and their actual keys.
//
// This storage system is at its heart a multiversioned key-value store which can
// track dependencies between different operations. It can also detect conflicts in
// multiple write operations and create branches instead of aborting such conlficts.
type Store struct {
	Data        []Node
	CommitIndex map[uint64]int
	KeyIndex    map[string][]int
	mu          sync.Mutex
}

// Init initializes all the fields in the Store struct, making the the storage system
// ready to use
func (s *Store) Init() {

	if len(s.Data) == 0 {
		s.CommitIndex = make(map[uint64]int)
		s.KeyIndex = make(map[string][]int)

		root := Node{
			commit:  0,
			parents: make([]uint64, 0),
			key:     "",
			value:   []byte(""),
		}
		root.parents = append(root.parents, math.MaxUint64)

		s.Data = append(s.Data, root)
		s.CommitIndex[0] = 0
		s.KeyIndex[""] = append(s.KeyIndex[""], 0)

	}
}

// can this generate math.MaxUint64? If so, that is a bug
func (s *Store) generateCommit() uint64 {
	return uint64(rand.Uint32())<<32 + uint64(rand.Uint32())
}

func (s *Store) Set(key string, value []byte, dependencies []uint64) (uint64, error) {
	// Allocate memory for new node
	node := Node{
		parents: dependencies,
		commit:  s.generateCommit(),
		key:     key,
		value:   value,
	}

	s.mu.Lock()
	defer s.mu.Unlock()

	// Add node to data structures
	s.Data = append(s.Data, node)
	s.CommitIndex[node.commit] = len(s.Data) - 1
	s.KeyIndex[key] = append(s.KeyIndex[key], len(s.Data)-1)

	// // Add new node as child to parent(s)
	// for _, dep := range dependencies {
	// 	_, ok := s.CommitIndex[dep]
	// 	if !ok {
	// 		return math.MaxUint64, errors.New("dependency does not exist")
	// 	}
	// 	parentNode := s.Data[s.CommitIndex[dep]]
	// 	// fmt.Printf("appending node %d as a child of %d\n", node.commit, parentNode.commit)
	// 	parentNode.children = append(parentNode.children, node.commit)
	// 	s.Data[s.CommitIndex[dep]] = parentNode
	// 	// fmt.Printf("%v\n", parentNode)

	// }
	return node.commit, nil
}

func (s *Store) GetbyCommit(commit uint64) (Node, error) {
	// fmt.Println("commit ", commit)
	if _, ok := s.CommitIndex[commit]; !ok {
		return Node{}, errors.New("CommitError: No node corresponding to this commit point exists")
	}

	return s.Data[s.CommitIndex[commit]], nil
}

func (s *Store) Count() (uint64, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	return uint64(len(s.Data)), nil
}

func (s *Store) GetbyKey(key string) ([]byte, bool, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	// if _, ok := s.KeyIndex[key]; !ok {
	// 	return []Node{}, errors.New("KeyError: No node corresponding to this key exists")
	// }

	// nodes := make([]Node, 0)
	// for _, node := range s.KeyIndex[key] {
	// 	nodes = append(nodes, s.Data[node])
	// }
	// return nodes, nil
	_, ok := s.KeyIndex[key]
	if ok {
		return s.Data[s.KeyIndex[key][0]].value, ok, nil
	}
	return []byte(""), ok, errors.New("KeyError: No node corresponding to this key exists")
}

// func (s *Store) Get(string key, uint64 commit) ([]Node, error) {
// 	if _, ok := s.KeyIndex[]; !ok {
// 		return nil, errors.New("")
// 	}

// 	// unknown commit, return all nodes corresponding to this key
// 	if commit == math.MaxUint64 {

// 	}
// }

func (s *Store) ResetConnection(key string) error {
	return nil
}

func (s *Store) Print() {
	for _, node := range s.Data {
		fmt.Printf("Commit Hash: %20d Key: %10s Value: %10s Parent: %v Children: %v\n", node.commit, node.key, node.value, node.parents, node.children)
	}
}

// GetTopologicalOrder performs a BFS on the dependency graph
// Append each note you visit to an output list
// The output list will contain the nodes of the graph
// in a topological order.
func (s *Store) GetTopologicalOrder() ([]Node, error) {
	root, err := s.GetbyCommit(0)
	if err != nil {
		return []Node{}, errors.New("Error in calling get() on root node")
	}
	outputList := make([]Node, 0)
	queue := make([]Node, 0)
	queue = append(queue, root)
	existenceMap := make(map[uint64]struct{})
	for {
		if len(queue) == 0 {
			break
		}
		currNode := queue[0]
		for _, child := range currNode.children {
			childNode, getErr := s.GetbyCommit(child)
			// fmt.Println("child key: ", childNode.key)
			if getErr != nil {
				return []Node{}, errors.New("Error in calling get() on a child node")
			}
			// append only if we have not come across this node before
			if _, ok := existenceMap[childNode.commit]; !ok {
				outputList = append(outputList, childNode)
				queue = append(queue, childNode)
				existenceMap[childNode.commit] = struct{}{}
			}

		}
		queue = queue[1:]
	}
	return outputList, nil
}

// func (s *Store) Flatten() {

// }
