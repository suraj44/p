// Package storage is an in-memory key-value stored backedn implemented using the map data structure
package storage

import (
	"sync"
)

type Storage struct {
	store map[string][]byte
	v     uint64
	mu    sync.Mutex
}

func New() *Storage {
	return &Storage{
		store: make(map[string][]byte),
	}
}

func (s *Storage) GetbyKey(key string) ([]byte, bool, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	val, ok := s.store[key]
	return val, ok, nil
}

func (s *Storage) Set(key string, value []byte) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.store[key] = value
	s.v++

	// fmt.Printf("set key %s to value %v. Size of this store: %d\n", key, s.store[key], s.v)

	return nil
}

func (s *Storage) Count() (uint64, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	count := s.v

	return count, nil
}

func (s *Storage) ResetConnection(key string) error {
	return nil
}
