module github.com/suraj44/graph-kv

go 1.14

require (
	github.com/goccy/go-graphviz v0.0.9 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
	github.com/golang/protobuf v1.5.2
	github.com/google/pprof v0.0.0-20210609004039-a478d1d731e9 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20210406231658-61c622dd7d50 // indirect
	github.com/orcaman/concurrent-map v0.0.0-20210501183033-44dafcb38ecc // indirect
	github.com/petermattis/goid v0.0.0-20180202154549-b0b1615b78e5 // indirect
	github.com/sasha-s/go-deadlock v0.2.0 // indirect
	github.com/sirupsen/logrus v1.8.0
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.27.1
)
