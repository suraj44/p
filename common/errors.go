package common

type Error string

func (e Error) Error() string { return string(e) }

const (
	ErrKey            = Error("keyError: No node corresponding to this key exists")
	ErrCommit         = Error("CommitError: No node corresponding to this commit point exists")
	ErrExcessAck      = Error("This ACK is excess, already recieved n-1 ACKs!")
	ErrCommitExists   = Error("There is already a node with this commit point present in the system")
	ErrAllValsAborted = Error("All values corresponding to this key have been aborted")
	ErrNoValue        = Error("No unaborted value for this key exists")
)
