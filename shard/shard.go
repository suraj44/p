package shard

import (
	"sync"

	"github.com/suraj44/graph-kv/store"
)

// Storage is a kv store
type Storage interface {
	// GetbyKey returns the value corresponding to key. Bool is used to indicate if the value is present.
	GetbyKey(key string, policyId int) (string, uint64, error)

	// // GetbyCommit returns a node (TODO: yet to be defined in this package) corresponding to a key. Bool is used to indicate if the value is present
	// GetbyCommit(commit uint64) ([]byte, bool, error)

	// Set creates a key-value pair
	Set(key string, value string, tid uint64, dependencies []uint64) (uint64, error)

	Save(commit uint64, key string, tid uint64, dependencies []uint64) error

	ReleaseBuffer(n int) []*store.NodeMetadata

	ConstructTree([]*store.NodeMetadata)

	Print()

	// // ResetConnection reinitializes the connection for the shard responsible for a given key
	// ResetConnection(key string) error

	// // Count returns the number of key-value pairs stored at this instance of the storage backend
	// Count() (uint64, error)
}

// Indexer maps keys to shards
// TODO: The number of shards must be elastic based on how much data is written to the ky-value store.
// Add functionality to add more servers
type Indexer interface {
	// SetBuckets sets the list of known buckets from which the indexer should select
	SetBuckets([]string) error

	// Choose returns a bucket for a given key
	Choose(key string) string

	// Buckets returns the list of known buckets
	Buckets() []string
}

// KVStore is a sharded key-value store
type KVStore struct {
	// consistent hash ring (change name to ring?)
	indexer Indexer
	Store   Storage

	// This lock is only used when selecting a shard for an operation. It is not
	// held when a call is made to the storage backend
	mu sync.Mutex
}

// Shard is a named backend store
// For now, all the shards will be on one server.
// TODO: Once the basic kv store functionality is setup, spread these shards across
// multiple machines.
type Shard struct {
	Name    string
	Backend Storage
}

// New returns a KV Store that uses indexer to shard keys across the provided shards
func New(indexer Indexer, shard Shard) *KVStore {
	kv := &KVStore{
		indexer: indexer,
		Store:   shard.Backend,
	}

	// for _, shard := range shards {
	// 	buckets = append(buckets, shard.Name)
	// 	kv.AddShard(shard.Name, shard.Backend)
	// }
	// kv.indexer.SetBuckets(buckets)
	return kv
}

// GetbyKey implements Storage.GetbyKey()
func (kv *KVStore) GetbyKey(key string, policy int) (string, uint64, error) {
	// kv.mu.Lock()

	// Use the indexer to find the shard corresponding to the given key
	// shard := kv.indexer.Choose(key)
	// fmt.Println(shard)

	// kv.mu.Unlock()

	return kv.Store.GetbyKey(key, policy) // store.GetbyKey(key, policy)
}

// Set implements Storage.Set()
func (kv *KVStore) Set(key string, value string, tid uint64, dependencies []uint64) (uint64, error) {
	// kv.mu.Lock()

	// shard := kv.indexer.Choose(key)
	// store := kv.stores["test_shard0"]
	// fmt.Println(shard)

	//kv.mu.Unlock()

	return kv.Store.Set(key, value, tid, dependencies)

}

func (kv *KVStore) Save(commit uint64, key string, tid uint64, dependencies []uint64) error {
	// kv.mu.Lock()

	// shard := kv.indexer.Choose(key)
	// store := kv.stores[shard]
	// // fmt.Println(shard)

	// kv.mu.Unlock()

	return kv.Store.Save(commit, key, tid, dependencies)

}

func (kv *KVStore) Flatten(list []*store.NodeMetadata) {
	// var store Storage

	// for _, s := range kv.stores {
	// 	store = s
	// 	break // assumption is that there will only be one shard / server for now
	// }

	kv.Store.ConstructTree(list)

	return
}

func (kv *KVStore) ReleaseBuffer(n int) []*store.NodeMetadata {
	// var store Storage

	// for _, s := range kv.stores {
	// 	store = s
	// 	break // assumption is that there will only be one shard / server for now
	// }

	return kv.Store.ReleaseBuffer(n)

}

// GetbyCommit implements Storage.GetbyCommit()
// how would this function work? Indexer chooses a shard using a key.
// How can I get to the same shard using a commit hash instead?
// func (kv *KVStore) GetbyCommit(commit uint64) ([]byte, bool, error) {
// 	kv.mu.Lock()

// 	// Use the indexer to find the shard corresponding to the given key
// 	shard := kv.indexer.Choose(key)
// 	store := kv.stores[shard]

// 	kv.mu.Unlock()

// 	return store.GetbyKey(key)
// }

// AddShard adds a shard to the KVStore from the list of known shards
func (kv *KVStore) AddShard(shard string, store Storage) {
	// kv.mu.Lock()
	// defer kv.mu.Unlock()

	// kv.stores[shard] = store
	return
}

// DeleteShard removes a shard from the list of known shards
func (kv *KVStore) DeleteShard(shard string) {
	// kv.mu.Lock()
	// defer kv.mu.Unlock()

	// delete(kv.stores, shard)
	return
}

// TODO: Functionality for 'expanding' the KVStore to more shards!
