package shard

// func TestShardedMemorykv(t *testing.T) {
// 	var shards []Shard
// 	nElements := 1000
// 	nShards := 10

// 	for i := 0; i < nShards; i++ {
// 		label := "test_shard" + strconv.Itoa(i)
// 		shards = append(shards, Shard{Name: label, Backend: store.New()})
// 	}

// 	indexer := indexer.New()

// 	kv := New(indexer, shards)

// 	for i := 0; i < nElements; i++ {
// 		kv.Set("test"+strconv.Itoa(i), []byte("value"+strconv.Itoa(i)))
// 	}

// 	for i, store := range kv.stores {
// 		count, _ := store.Count()
// 		fmt.Printf("size of store %s is %d\n", i, count)
// 	}

// }

// func TestShardedGraphkv(t *testing.T) {
// 	var shards []Shard
// 	nElements := 1000
// 	nShards := 10

// 	for i := 0; i < nShards; i++ {
// 		label := "test_shard" + strconv.Itoa(i)
// 		store := graph.Store{}
// 		store.Init()
// 		shards = append(shards, Shard{Name: label, Backend: &store})
// 	}

// 	indexer := indexer.New()

// 	kv := New(indexer, shards)

// 	var dep uint64
// 	var err error
// 	dep = 0
// 	for i := 0; i < nElements; i++ {
// 		dep, err = kv.Set("test"+strconv.Itoa(i), []byte("value"+strconv.Itoa(i)), []uint64{dep})
// 		if err != nil {
// 			t.Errorf("Set(): Expected nil but got %s\n", err)
// 		}
// 	}

// 	for i, store := range kv.stores {
// 		count, _ := store.Count()
// 		fmt.Printf("size of store %s is %d\n", i, count)
// 	}

// }
