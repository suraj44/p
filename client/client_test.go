package client

import (
	"math/rand"
	"testing"
	"time"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
)

func RandStringBytesMask(n int) string {
	b := make([]byte, n)
	for i := 0; i < n; {
		if idx := int(rand.Int63() & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i++
		}
	}
	return string(b)
}

// func TestSetGet(t *testing.T) {
// 	c, _ := NewClient("127.0.0.1:5000")
// 	err := c.Init()
// 	if err != nil {
// 		t.Errorf("init: wanted nil, got %s", err.Error())
// 	}

// 	txn := c.NewClientTxn()

// 	key := "a"
// 	value := []byte("5")
// 	txn.Write(key, string(value), []uint64{0})

// 	key = "z"
// 	value = []byte("5")

// 	_, err = c.Set(key, string(value), []uint64{0})

// 	key = "k"
// 	value = []byte("5")

// 	_, err = c.Set(key, string(value), []uint64{0})

// 	key = "g"
// 	_, err = c.Set(key, string(value), []uint64{0})

// 	key = "fkadf"
// 	_, err = c.Set(key, string(value), []uint64{0})

// 	if err != nil {
// 		t.Errorf("Set(): expected nil got %s\n", err.Error())
// 		return
// 	}
// 	resp, err := c.GetbyKey(key)
// 	if err != nil {
// 		t.Errorf("GetbyKey(): expected nil got %s\n", err.Error())
// 		return
// 	}
// 	if !reflect.DeepEqual(resp.Value, string(value)) {
// 		t.Errorf("GetbyKey(): expected %v got %s\n", string(value), resp.Value)
// 		return
// 	}
// }

func TestComplex(t *testing.T) {
	c, _ := NewClient([]string{"10.0.1.4:5000"})

	value := []byte("5")
	txn := c.NewClientTxn()
	commitA, _ := txn.Write("a", string(value), []uint64{0})
	txn.Commit()
	txn = c.NewClientTxn()
	commitB, _ := txn.Write("b", string(value), []uint64{0})
	txn.Commit()
	txn = c.NewClientTxn()
	txn.Write("c", string(value), []uint64{0})
	txn.Commit()
	txn = c.NewClientTxn()
	commitD, _ := txn.Write("d", string(value), []uint64{0})
	txn.Commit()
	txn = c.NewClientTxn()
	commitD1, _ := txn.Write("d", "d1", []uint64{commitA, commitD})
	txn.Commit()
	txn = c.NewClientTxn()
	commitD2, _ := txn.Write("d", "d2", []uint64{commitA, commitB, commitD})
	txn.Commit()
	txn = c.NewClientTxn()
	commitF, _ := txn.Write("e", string(value), []uint64{commitD1})
	txn.Commit()
	txn = c.NewClientTxn()
	commitG, _ := txn.Write("f", string(value), []uint64{commitD2})
	txn.Commit()
	time.Sleep(2 * time.Second)
	txn = c.NewClientTxn()
	txn.Write("g", string(value), []uint64{commitG, commitF})
	txn.Commit()

	time.Sleep(2 * time.Second)
	c.Print()

}

// func BenchmarkSet(b *testing.B) {
// 	rand.Seed(time.Now().Unix())
// 	nServers := 5
// 	var clients []*Client

// 	for i := 0; i < nServers; i++ {
// 		c, _ := NewClient([]string{"127.0.0.1:5000"})
// 		clients = append(clients, c)
// 	}

// 	for n := 0; n < b.N; n++ {
// 		key := RandStringBytesMask(10)
// 		value := []byte("5")

// 		// pick a random server to send a request to
// 		_, err := clients[rand.Intn(len(clients))].Set(key, string(value), []uint64{0})
// 		if err != nil {
// 			b.Errorf("Set(): expected nil got %s\n", err.Error())
// 			return
// 		}

// 	}
// }
