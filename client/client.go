package client

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/suraj44/graph-kv/db/db"
	"github.com/suraj44/graph-kv/indexer"
	"github.com/suraj44/graph-kv/shard"
	"google.golang.org/grpc"
)

type Client struct {
	// Address of the server that the client
	// will connect to
	// serverAddress string
	serverList    []string
	indexer       shard.Indexer
	serverConnMap map[string]*grpc.ClientConn

	// Client connection with the serverAddress
	// conn *grpc.ClientConn

	dbConn       db.DbServiceClient
	count        int
	totalLatency time.Duration
}

type Options struct {
	// host:port address
	Addr string
}

func NewClient(serverList []string) (*Client, error) {
	c := &Client{
		serverList:    serverList,
		serverConnMap: make(map[string]*grpc.ClientConn),
	}
	for _, server := range serverList {
		conn, err := grpc.Dial(server, grpc.WithInsecure())
		if err != nil {
			fmt.Println("error creating grpc channel")
			return nil, err
		}
		c.serverConnMap[server] = conn
	}
	// conn, err := grpc.Dial(c.serverAddress, grpc.WithInsecure())
	// if err != nil {
	// 	fmt.Println("error creating grpc channel")
	// 	return nil, err
	// }
	fmt.Println("no error while creating grpc")
	c.count = 0

	cHash := indexer.New()
	c.indexer = cHash
	c.indexer.SetBuckets(serverList)

	return c, nil
}

// func (c *Client) Init() error {
// 	conn, err := grpc.Dial(c.serverAddress, grpc.WithInsecure())
// 	if err != nil {
// 		return err
// 	}
// 	c.conn = conn

// 	c.dbConn = db.NewDbServiceClient(c.conn)

// 	return nil
// }

// func (c *Client) Close() {
// 	c.conn.Close()
// }

func (c *Client) Set(key string, value string, tId uint64, dependencies []uint64) (*db.SetResponse, error) {
	ctx := context.Background()
	server := c.indexer.Choose(key)
	dbConn := db.NewDbServiceClient(c.serverConnMap[server])
	resp, err := dbConn.Set(ctx, &db.SetRequest{
		Key:          key,
		Value:        value,
		Dependencies: dependencies,
		TId:          tId,
	})
	return resp, err
}

func (c *Client) GetbyKey(key string) (*db.GetResponse, error) {
	ctx := context.Background()
	server := c.indexer.Choose(key)
	dbConn := db.NewDbServiceClient(c.serverConnMap[server])
	resp, err := dbConn.GetbyKey(ctx, &db.GetRequest{
		Key: key,
	})
	return resp, err
}

func (c *Client) Print() {
	ctx := context.Background()
	for _, server := range c.serverList {
		dbConn := db.NewDbServiceClient(c.serverConnMap[server])
		dbConn.Print(ctx, &db.Empty{})
	}
}

func (c *Client) Done() {
	ctx := context.Background()
	for _, server := range c.serverList {
		dbConn := db.NewDbServiceClient(c.serverConnMap[server])
		dbConn.Done(ctx, &db.Empty{})
	}
}

type ClientTxn struct {
	client   *Client
	tId      uint64
	writeset []string
}

func (c *Client) generateCommit() uint64 {
	// TODO: Check if generated commit is already used??
	return uint64(rand.Uint32())<<32 + uint64(rand.Uint32())
}

func (c *Client) NewClientTxn() *ClientTxn {
	// c.count++
	tId := c.generateCommit()

	t := &ClientTxn{
		client: c,
		tId:    tId,
	}

	return t
}

func (c *ClientTxn) Read(key string) (string, uint64, string) {
	ctx := context.Background()
	server := c.client.indexer.Choose(key)
	dbConn := db.NewDbServiceClient(c.client.serverConnMap[server])
	resp, _ := dbConn.GetbyKey(ctx, &db.GetRequest{
		Key: key,
	})
	// if err != nil || resp.Error != "" {
	// 	fmt.Println(resp)
	// 	fmt.Println("ERROR", resp.Error) // abort the txn from the client side?
	// }
	// if resp.Error != "" {
	// 	s := fmt.Sprintf("key: %s ", key)
	// 	panic(s + resp.Error)
	// }
	return string(resp.Value), resp.Commit, resp.Error
}

func (c *ClientTxn) Write(key, val string, dependencies []uint64) (uint64, bool) {
	ctx := context.Background()
	server := c.client.indexer.Choose(key)
	dbConn := db.NewDbServiceClient(c.client.serverConnMap[server])
	resp, err := dbConn.Set(ctx, &db.SetRequest{
		Key:          key,
		Value:        val,
		TId:          c.tId,
		Dependencies: dependencies,
	})
	// commit, _ := s.store.Set(key, val, dependencies)
	if err != nil {
		panic(err)
	}
	c.writeset = append(c.writeset, key)
	return resp.Commit, true
}

func (c *ClientTxn) Commit() bool {
	// No need to commit read-only txns on the server
	if len(c.writeset) == 0 {
		return true
	}
	// If this is a multi-shard txn, we must send a 'commit' message to
	// all shards involved in the txn.
	var shards []string
	for _, key := range c.writeset {
		ctx := context.Background()
		server := c.client.indexer.Choose(key)
		shards = append(shards, server)
		dbConn := db.NewDbServiceClient(c.client.serverConnMap[server])
		_, err := dbConn.Commit(ctx, &db.TxnRequest{
			TId: c.tId,
		})
		if err != nil {
			panic(err)
		}
	}
	fmt.Printf("shards involved in txn %d were %v\n", c.Tid(), shards)

	return true
}

func (c *ClientTxn) Tid() uint64 {
	return c.tId
}

func (c *Client) CommitTxn(tId uint64) bool {
	ctx := context.Background()
	server := c.indexer.Choose("0")
	dbConn := db.NewDbServiceClient(c.serverConnMap[server])
	_, err := dbConn.Commit(ctx, &db.TxnRequest{
		TId: tId,
	})
	if err != nil {
		panic(err)
	}
	return true
}

func (c *ClientTxn) Abort() {
	ctx := context.Background()
	server := c.client.indexer.Choose("0")
	dbConn := db.NewDbServiceClient(c.client.serverConnMap[server])
	_, err := dbConn.Abort(ctx, &db.TxnRequest{
		TId: c.tId,
	})
	if err != nil {
		panic(err)
	}

}
