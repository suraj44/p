package main

import (
	"context"

	"github.com/suraj44/graph-kv/db/db"
	"github.com/suraj44/graph-kv/store"
)

type Server struct {
	store *store.Store
	addr  string
}

func NewServer(addr string, epochSize uint64) *Server {
	return &Server{
		store: store.NewStore(epochSize),
		addr:  addr,
	}
}

// func (s *Server) Set(ctx context.Context, req *db.SetRequest) (result *db.SetResponse, err error) {
// 	commit, err := s.store.Set(req.Key, string(req.Value), req.Dependencies)

// 	if err != nil {
// 		return = &db.SetResponse{
// 			Commit: 0,
// 			Error: "",
// 		}
// 	} else {
// 		result = &db.SetResponse{
// 			Commit: commit,
// 			Error:  "",
// 		}
// 	}
// 	return result, nil
// }

func (s *Server) Set(ctx context.Context, req *db.SetRequest) (result *db.SetResponse, err error) {
	// fmt.Println("set", req.Key)
	commit, _ := s.store.Set(req.Key, string(req.Value), req.TId, req.Dependencies)

	// TODO: figure out why using err from kv.Set() results in SIGSEGV
	result = &db.SetResponse{
		Commit: commit,
		Error:  "",
	}
	return result, err

}

func (s *Server) GetbyKey(ctx context.Context, req *db.GetRequest) (result *db.GetResponse, err error) {
	value, commit, err := s.store.GetbyKey(req.Key, 0)
	// TODO: add commit uint64 to response
	// is exists superfluous if we are returning error??
	// main aim to to be able to run a retwis experiment across client- server by tonight!
	result = &db.GetResponse{
		Value:  value,
		Commit: commit,
		Error:  "",
	}
	return result, nil
}
