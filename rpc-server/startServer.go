package main

import (
	"flag"
	"log"
	"net"

	"github.com/suraj44/graph-kv/db/db"
	"google.golang.org/grpc"
)

var serverAddress = flag.String("address", "10.0.3.11:9000", "port number to launch server")
var epochSize = flag.Uint64("epoch", 1000, "size of epoch to perform flattening")

func main() {
	flag.Parse()
	address := *serverAddress
	lis, err := net.Listen("tcp", address)

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	log.Println("Server listen at", address)

	server := NewServer(address, *epochSize)
	grpcServer := grpc.NewServer()
	db.RegisterDbServiceServer(grpcServer, server)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
