package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"sort"
	"sync"
	"sync/atomic"

	"github.com/suraj44/graph-kv/common"
	"github.com/suraj44/graph-kv/db/db"
	"github.com/suraj44/graph-kv/indexer"
	"github.com/suraj44/graph-kv/shard"
	"google.golang.org/grpc"
)

const ( // iota is reset to 0
	COLLECT     = iota // c0 == 0
	DISSEMINATE = iota // c1 == 1
	RECONCILE   = iota // c2 == 2
	FLATTEN     = iota
	WAIT        = iota
)

type Node struct {
	dependencies      []uint64
	parent            *Node
	commit            uint64
	key               string
	value             string
	children          []*Node
	flattenedChildren []*Node
	tId               uint64
	flattened         bool
	lineage           []uint32
	inDegree          int
	depth             int
}

type NodeMetadata struct {
	Commit       uint64
	Key          string
	Tid          uint64
	Dependencies []uint64
}

type Txn struct {
	abort    bool
	writeSet []*Node
}

type BranchElement struct {
	leaf   *Node
	length int
}

type Server struct {
	keyIndex         map[string][]*Node
	km               sync.RWMutex
	txnIndex         map[uint64]*Txn
	tm               sync.RWMutex
	commitIndex      map[uint64]*Node
	cm               sync.RWMutex
	commitPointerMap map[uint64]*Node
	tmpMap           map[*Node]struct{}
	txnStagingArea   map[uint64]struct{}
	tSm              sync.RWMutex
	snapshotChan     chan *Node
	snapshot         []*Node
	sm               sync.Mutex
	counter          uint32
	epochSize        int
	abortCount       int
	flattening       int32
	finish           bool
	peers            []string
	indexer          shard.Indexer
	stage            uint32
	localSnapshot    []*Node
	globalSnapshot   []NodeMetadata
	finalSnapshot    []*Node
	orphanNodes      []NodeMetadata
	gSm              sync.Mutex
	recvCount        uint32
	ackCount         uint32
	// useful info for debugging and analysis
	totalReads      uint32
	abortedReads    uint32
	clientAborts    uint32
	clientCommits   uint32
	wwAbort         uint32
	cascadingAborts uint32

	branches []*BranchElement
	addr     string
}

func (s *Server) createRootNode() {
	root := &Node{
		commit:       0,
		dependencies: []uint64{},
		key:          "",
		value:        "",
		tId:          0,
		lineage:      []uint32{0},
		flattened:    true,
		inDegree:     0,
		depth:        0,
	}
	s.commitPointerMap[0] = root
	txn := &Txn{
		abort:    false,
		writeSet: []*Node{root},
	}

	branch := &BranchElement{
		leaf:   root,
		length: 1,
	}
	s.branches = append(s.branches, branch)
	s.txnIndex[0] = txn
	s.snapshot = append(s.snapshot, root)
}

func NewServer(addr string, peers []string, epochSize int) *Server {
	s := &Server{
		keyIndex:         make(map[string][]*Node),
		txnIndex:         make(map[uint64]*Txn),
		commitPointerMap: make(map[uint64]*Node),
		commitIndex:      make(map[uint64]*Node),
		txnStagingArea:   make(map[uint64]struct{}),
		addr:             addr,
		epochSize:        epochSize,
		abortCount:       0,
		snapshotChan:     make(chan *Node),
		flattening:       0,
		counter:          0,
		tmpMap:           make(map[*Node]struct{}),
		finish:           false,
		stage:            COLLECT,
		indexer:          indexer.New(),
		peers:            peers,
		recvCount:        0,
		ackCount:         0,
		totalReads:       0,
		clientAborts:     0,
		clientCommits:    0,
		abortedReads:     0,
		wwAbort:          0,
		cascadingAborts:  0,
	}

	// Register the peers for the consistent hashing logic
	s.indexer.SetBuckets(s.peers)

	s.createRootNode()
	go s.backgroundRoutine()
	// go s.backgroundFlattening()
	return s
}
func (s *Server) performFlattening() int {
	for {
		if atomic.CompareAndSwapInt32(&s.flattening, 0, 1) {
			break
		}
	}
	// epoch := s.getEpoch()
	orderedEpoch := s.getTopologicalSort(s.finalSnapshot)
	s.flatten(orderedEpoch)
	atomic.CompareAndSwapInt32(&s.flattening, 1, 0)
	return len(s.finalSnapshot)
}

func (s *Server) backgroundRoutine() {
	// var count int = 1
	for {
		select {
		case n := <-s.snapshotChan:
			// count += 1
			s.sm.Lock()
			s.snapshot = append(s.snapshot, n)
			s.sm.Unlock()
			log.Println("snapshot length of", s.addr, len(s.snapshot))
			if len(s.snapshot) >= s.epochSize || s.finish {
				go s.StateMachine()
			}
		}
	}

}

// func (s *Server) backgroundFlattening() {
// 	for {
// 		if len(s.snapshot) >= s.epochSize || s.finish {
// 			for {
// 				{
// 					if len(s.snapshot) < s.epochSize {
// 						break
// 					}
// 					s.performFlattening()
// 				}
// 			}
// 		}
// 	}

// }

func (s *Server) StateMachine() bool {
	// tryLock()

	if !atomic.CompareAndSwapUint32(&s.stage, COLLECT, DISSEMINATE) {
		fmt.Printf("%s: Expected stage 0 got stage %d. Could not start dissemination.\n", s.addr, s.stage)
		return false
	}
	log.Println(s.addr, "Started state machine")
	s.broadcast()
	// log.Println(s.addr, "Done broadcasting")
	// wait to receive metadata from all peers
	peerCount := len(s.peers) - 1
	for {
		if int(s.recvCount) == len(s.peers)-1 {
			break
		}
	}
	// log.Println(s.addr, "Received all metadata")
	// Move to flattening stage
	s.recvCount = 0
	for {
		if atomic.CompareAndSwapUint32(&s.stage, DISSEMINATE, FLATTEN) {
			break
		}
	}
	// insert buffered data into our instance of the storage system and empty the buffer in the process
	// log.Printf("%s: saving nodes %v\n", s.addr, s.buffer)
	s.savePeerNodes(s.globalSnapshot)
	s.finalSnapshot = append(s.finalSnapshot, s.localSnapshot...)
	// s.gSm.Lock()
	// s.globalSnapshot = append(s.globalSnapshot, s.serializeSnapshot(s.localSnapshot)...)
	// s.gSm.Unlock()

	s.addOldOrphans()
	s.removeOrphans()

	s.performFlattening()

	s.localSnapshot = []*Node{}
	s.globalSnapshot = []NodeMetadata{}
	s.finalSnapshot = []*Node{}

	// s.buffer = s.GetTopologicalOrder(s.buffer)

	// // flatten
	// // Flatten(s.buffer)
	// // log.Printf("%s: Buffer: %v\n", s.addr, s.buffer)
	// // log.Println(s.addr, "Flattening...")
	// s.store.ConstructTree(s.buffer)
	// //fmt.Println("flatten return:", ret)
	// s.buffer = []*store.NodeMetadata{}
	// move to stage 3
	for {
		if atomic.CompareAndSwapUint32(&s.stage, FLATTEN, WAIT) {
			break
		}
	}
	// log.Println(s.addr, "Notifying..")
	s.notify()
	// wait for n-1 ACKs
	for {
		if int(s.ackCount) == peerCount {
			break
		}
	}
	log.Println(s.addr, "Received all ACKs. State machine done!")
	// move back to stage 0
	// s.recvCount = 0
	s.ackCount = 0
	for {
		if atomic.CompareAndSwapUint32(&s.stage, WAIT, COLLECT) {
			break
		}
	}

	return true
}

func (s *Server) addOldOrphans() {
	s.gSm.Lock()
	s.globalSnapshot = append(s.globalSnapshot, s.orphanNodes...)
	s.gSm.Unlock()
	s.orphanNodes = s.orphanNodes[:0]
}

func (s *Server) removeOrphans() {
	i := 0 // output index
	for _, node := range s.globalSnapshot {
		orphan := false
		for _, parent := range node.Dependencies {
			s.cm.RLock()
			if _, ok := s.commitIndex[parent]; !ok {
				orphan = true
				s.orphanNodes = append(s.orphanNodes, node)
				break
			}
			s.cm.RUnlock()
		}
		if !orphan {
			s.globalSnapshot[i] = node
			i++
		}
	}
	println("Size of orphans is", len(s.orphanNodes))

	// prevent memory leak
	// for j := i; j < len(s.buffer); j++ {
	// 	s.buffer[j] = nil
	// }
	s.globalSnapshot = s.globalSnapshot[:i]
}

func (s *Server) savePeerNodes(list []NodeMetadata) {
	// fmt.Println("started save")
	for _, node := range list {
		node := s.save(node.Commit, node.Key, node.Tid, node.Dependencies)
		s.finalSnapshot = append(s.finalSnapshot, node)
		// s.epochChan <- 0
	}
	// fmt.Println("finished save")
}

func (s *Server) broadcast() {
	localSnapshot := s.getEpoch()
	s.localSnapshot = localSnapshot

	broadcastData := make([]*db.DisseminateRequest, len(localSnapshot))

	for i := range localSnapshot {
		d := &db.DisseminateRequest{
			Commit:       localSnapshot[i].commit,
			Dependencies: localSnapshot[i].dependencies,
			Key:          localSnapshot[i].key,
			Tid:          localSnapshot[i].tId,
		}
		broadcastData[i] = d
	}

	for _, server := range s.peers {
		if server != s.addr {
			// RPC to send data to peer
			ctx := context.Background()
			conn, err := grpc.Dial(server, grpc.WithInsecure())
			if err != nil {
				panic(err)
			}
			client := db.NewDbServiceClient(conn)
			// TODO: add error check
			client.Disseminate(ctx, &db.DisseminateRequestList{
				Metadata: broadcastData,
			})
			conn.Close()
		}
	}

}

func (s *Server) serializeSnapshot(snapshot []*Node) []NodeMetadata {
	ret := make([]NodeMetadata, len(snapshot))

	for i := range snapshot {
		d := NodeMetadata{
			Commit:       snapshot[i].commit,
			Dependencies: snapshot[i].dependencies,
			Key:          snapshot[i].key,
			Tid:          snapshot[i].tId,
		}
		ret[i] = d
	}

	return ret
}

func (s *Server) notify() {
	for _, server := range s.peers {
		if server != s.addr {
			// RPC to send data to peer
			ctx := context.Background()
			conn, err := grpc.Dial(server, grpc.WithInsecure())
			if err != nil {
				panic(err)
			}
			client := db.NewDbServiceClient(conn)
			// TODO: add error check
			client.Notify(ctx, &db.ACK{
				Addr: s.addr,
			})
			conn.Close()
		}
	}

}

// func (s *Server) deserializeSnapshot(receivedSnapshot []NodeMetadata) []*Node {

// }

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}

func (s *Server) getEpoch() []*Node {
	var ret []*Node
	var buffer []*Node
	snapLen := len(s.snapshot)
	iterRange := min(s.epochSize, snapLen)
	for i := 0; i < iterRange; i++ {
		s.sm.Lock()
		node := s.snapshot[0]
		s.snapshot = s.snapshot[1:]
		s.sm.Unlock()
		if node == nil {
			panic("node is nil")
		}
		s.commitPointerMap[node.commit] = node
		ret = append(ret, node)
	}

	var problem int = 0

	for {
		i := 0 // output index
		problem = 0
		for _, node := range ret {
			// canAppend := true
			ret[i] = node
			i++
			// for _, dep := range node.dependencies {
			// 	if _, ok := s.commitPointerMap[dep]; !ok {
			// 		canAppend = false
			// 		break
			// 	}
			// }
			// if !canAppend {
			// 	delete(s.commitPointerMap, node.commit)
			// 	buffer = append(buffer, node)
			// 	problem++
			// } else {

			// }
		}

		// TODO: should we fix potential memory leaks here?
		ret = ret[:i]
		if problem == 0 {
			break
		}
	}

	s.snapshot = append(buffer, s.snapshot...)

	return ret
}

func (s *Server) getTopologicalSort(graph []*Node) []*Node {
	var queue []*Node
	var final []*Node
	children := make(map[*Node]struct{})
	visited := make(map[*Node]struct{})
	snapshotMap := make(map[*Node]struct{})

	// sort the order in which nodes are processed. this makes sure
	// that the same tree will be built on each shard
	sort.Slice(graph, func(i, j int) bool {
		return graph[i].commit < graph[j].commit
	})

	for _, node := range graph {
		// s.commitPointerMap[node.commit] = node
		snapshotMap[node] = struct{}{}
	}

	// var prevEpochParents []*Node
	// addedParentMap := make(map[*Node]struct{})

	for _, node := range graph {
		for _, parent := range node.dependencies {
			pNode, ok := s.commitPointerMap[parent]
			if !ok {
				panic("parent node does not exist in storage system!")
			}
			// if _, ok := snapshotMap[pNode]; !ok { // if one of the parents are not in the current epoch, add them to the queue for processing their children
			// 	node.inDegree++
			// }
			pNode.children = append(pNode.children, node)
		}
	}

	for _, node := range graph {
		for _, child := range node.children {
			if _, ok := snapshotMap[child]; ok {
				child.inDegree++
			}
			if _, ok := children[child]; !ok {
				children[child] = struct{}{}
			}
		}
	}

	for _, node := range graph {
		if node.inDegree == 0 {
			queue = append(queue, node)
			final = append(final, node)
			visited[node] = struct{}{}
		}
	}

	for {
		if len(queue) == 0 {
			break
		}

		currNode := queue[0]
		queue = queue[1:]

		for _, child := range currNode.children {
			if _, ok := snapshotMap[child]; !ok {
				continue
			}
			// if _, ok := visited[child]; ok { // || child.flattened {
			// 	continue
			// }
			child.inDegree--
			if child.inDegree == 0 {
				queue = append(queue, child)
				visited[child] = struct{}{}
				final = append(final, child)
			}

		}
	}
	// fmt.Println("visited: ", len(visited), "snapshot:", len(snapshotMap), "final:", len(final))

	if len(visited) != len(snapshotMap) {

		panic("we have not visited all nodes in the current epoch!")
	}

	return final

}

func (s *Server) generateCommit() uint64 {
	// TODO: Check if generated commit is already used??
	return uint64(rand.Uint32())<<32 + uint64(rand.Uint32())
}

func (s *Server) addToWriteSet(node *Node, tId uint64) {
	s.tm.Lock()
	defer s.tm.Unlock()

	if t, ok := s.txnIndex[tId]; ok {
		t.writeSet = append(t.writeSet, node)
	} else {
		t := &Txn{
			abort:    false,
			writeSet: []*Node{node},
		}
		s.txnIndex[tId] = t
	}
}

func (s *Server) Set(ctx context.Context, req *db.SetRequest) (result *db.SetResponse, err error) {
	// fmt.Println("set", req.Key)
	s.km.Lock()
	s.cm.Lock()
	defer s.km.Unlock()
	defer s.cm.Unlock()
	node := &Node{
		commit:       s.generateCommit(),
		dependencies: req.Dependencies,
		key:          req.Key,
		value:        req.Value,
		tId:          req.TId,
		parent:       nil,
		inDegree:     0,
		depth:        0,
	}

	// s.km.Lock()
	s.keyIndex[node.key] = append(s.keyIndex[node.key], node) // prependNode(s.keyIndex[node.key], node) // append([]*Node{node}, s.keyIndex[node.key]...)
	// s.km.Unlock()

	// s.cm.Lock()
	s.commitIndex[node.commit] = node
	// s.cm.Unlock()

	s.addToWriteSet(node, req.TId)

	// s.snapshotChan <- node
	// if node != nil {
	// 	s.snapshotChan <- node
	// }
	if node == nil {
		panic("set(): node is nil!!!!")
	}
	s.tSm.Lock()
	_, ok := s.txnStagingArea[req.TId]
	if !ok {
		s.txnStagingArea[req.TId] = struct{}{}
	}
	s.tSm.Unlock()

	// go func() {
	// 	if node != nil {
	// 		s.snapshotChan <- node
	// 	}
	// }()

	result = &db.SetResponse{
		Commit: node.commit,
		Error:  "",
	}
	return result, err

}
func (s *Server) save(commit uint64, key string, tid uint64, dependencies []uint64) *Node {
	// fmt.Println("set", req.Key)
	s.km.Lock()
	s.cm.Lock()
	defer s.km.Unlock()
	defer s.cm.Unlock()
	node := &Node{
		commit:       commit,
		dependencies: dependencies,
		key:          key,
		tId:          tid,
		parent:       nil,
		inDegree:     0,
		depth:        0,
	}

	// s.km.Lock()
	s.keyIndex[node.key] = append(s.keyIndex[node.key], node) // prependNode(s.keyIndex[node.key], node) // append([]*Node{node}, s.keyIndex[node.key]...)
	// s.km.Unlock()

	// s.cm.Lock()
	s.commitIndex[node.commit] = node
	s.commitPointerMap[node.commit] = node
	// s.cm.Unlock()

	s.addToWriteSet(node, tid)

	// s.snapshotChan <- node
	// if node != nil {
	// 	s.snapshotChan <- node
	// }
	if node == nil {
		panic("save(): node is nil!!!!")
	}

	return node

	// go func() {
	// 	if node != nil {
	// 		s.snapshotChan <- node
	// 	}
	// }()

}

func (s *Server) GetbyKey(ctx context.Context, req *db.GetRequest) (result *db.GetResponse, err error) {
	// fmt.Println("get")
	atomic.AddUint32(&s.totalReads, 1)
	s.km.RLock()
	defer s.km.RUnlock()
	ret, ok := s.keyIndex[req.Key]

	if ok {
		l := len(ret)
		allAborts := true
		flattenBool := false
		foundDirtyVersion := false
		var flattenedVersion *Node = nil
		var dirtyVersion *Node = nil

		for i := l - 1; i >= 0; i-- {
			if !s.checkTxnAbort(ret[i].tId) {
				allAborts = false
				if ret[i].flattened && !flattenBool {
					flattenBool = true
					flattenedVersion = ret[i]
				}
				if !ret[i].flattened && !foundDirtyVersion {
					foundDirtyVersion = true
					dirtyVersion = ret[i]
				}
			}
		}
		if allAborts {
			//return error
			atomic.AddUint32(&s.abortedReads, 1)
			result = &db.GetResponse{
				Value:  "",
				Commit: 0,
				Error:  string(common.ErrNoValue),
			}

		} else if !flattenBool {
			// traverse and return latest dirty version
			result = &db.GetResponse{
				Value:  dirtyVersion.value,
				Commit: dirtyVersion.commit,
				Error:  "",
			}
		} else {
			// return flattenedVersion!
			result = &db.GetResponse{
				Value:  flattenedVersion.value,
				Commit: flattenedVersion.commit,
				Error:  "",
			}

		}

		// for i := len(ret) - 1; i >= 0; i-- {
		// 	if !s.checkAbort(ret[i]) {
		// 		node = ret[i]
		// 		break
		// 	}
		// }
		// if node == nil {
		// 	result = &db.GetResponse{
		// 		Value:  "",
		// 		Commit: 0,
		// 		Error:  string(common.ErrNoValue),
		// 	}
		// } else {
		// 	result = &db.GetResponse{
		// 		Value:  node.value,
		// 		Commit: node.commit,
		// 		Error:  "",
		// 	}
		// }
		return result, nil

	}
	result = &db.GetResponse{
		Value:  "",
		Commit: 0,
		Error:  string(common.ErrKey),
	}
	return result, nil
}

func (s *Server) Commit(ctx context.Context, req *db.TxnRequest) (result *db.Empty, err error) {
	// move txn from staging around to bg process, same flow as old
	s.tSm.Lock()
	if _, ok := s.txnStagingArea[req.TId]; ok {
		delete(s.txnStagingArea, req.TId)
		// TODO: is this correct? or should we hold the read lock and <- using a goroutine?
		s.tm.RLock()
		ws := s.txnIndex[req.TId].writeSet
		for _, node := range ws {
			go func(node *Node) {
				s.snapshotChan <- node
			}(node)
		}
		s.tm.RUnlock()
		atomic.AddUint32(&s.clientCommits, 1)
	}
	s.tSm.Unlock()

	return &db.Empty{}, nil
}

func (s *Server) Abort(ctx context.Context, req *db.TxnRequest) (result *db.Empty, err error) {
	// mark txn as aborted, remove from staging area... what about others that read this dirty data?- TODO: write a test for this!
	s.tSm.Lock()
	if _, ok := s.txnStagingArea[req.TId]; ok {
		delete(s.txnStagingArea, req.TId)
		s.tSm.Unlock()
		s.abort(req.TId)
		return &db.Empty{}, nil
	}
	s.tSm.Unlock()
	atomic.AddUint32(&s.clientAborts, 1)

	return &db.Empty{}, nil
}

func (s *Server) checkTxnAbort(tId uint64) bool {
	s.tm.RLock()
	defer s.tm.RUnlock()

	t, ok := s.txnIndex[tId]
	if !ok {
		panic("txn does not exist!")
	}
	return t.abort
}
func (s *Server) checkAbortOnline(node *Node) bool {
	// s.tm.RLock()
	// defer s.tm.RUnlock()
	if s.checkTxnAbort(node.tId) {
		return true
	}

	// for _, dep := range node.dependencies {
	// 	s.cm.RLock()
	// 	depNode, ok := s.commitIndex[dep]
	// 	s.cm.RUnlock()
	// 	if !ok {
	// 		panic("dependency not in commitIndex map!")
	// 	}
	// 	if s.checkTxnAbort(depNode.tId) {
	// 		return true
	// 	}
	// }

	return false
}

func (s *Server) checkAbort(node *Node) bool {
	// s.tm.RLock()
	// defer s.tm.RUnlock()
	if s.checkTxnAbort(node.tId) {
		return true
	}

	for _, dep := range node.dependencies {
		depNode, ok := s.commitPointerMap[dep]
		if !ok {
			panic("dependency not in map! something is wrong with topological order")
		}
		if s.checkTxnAbort(depNode.tId) {
			return true
		}
	}

	return false
}

func (s *Server) abort(tId uint64) bool {
	var ret bool
	s.tm.Lock()

	t, ok := s.txnIndex[tId]

	if !ok {
		panic("txn does not exist!")
	}
	ret = !t.abort
	if !t.abort {
		t.abort = true
		s.abortCount++
	}
	s.tm.Unlock()

	return ret

	// call cascade abort here

}

func (s *Server) cascadingAbort(tId uint64) {
	var topMostNode *Node = nil
	s.tm.RLock()
	for _, node := range s.txnIndex[tId].writeSet {
		if topMostNode == nil || (node.flattened && node.depth < topMostNode.depth) {
			topMostNode = node
		}
	}
	s.tm.RUnlock()

	// Search and fix
	queue := []*Node{}
	queue = append(queue, topMostNode.flattenedChildren...)

	for {
		if len(queue) == 0 {
			break
		}
		currNode := queue[0]
		queue = queue[1:]

		if s.abort(currNode.tId) {
			atomic.AddUint32(&s.cascadingAborts, 1)
		}

		// traverse its children!
		queue = append(queue, currNode.flattenedChildren...)
	}

}

func (s *Server) compareLineage(a, b []uint32) bool {
	var big, small []uint32
	if len(a) >= len(b) {
		big = a
		small = b
	} else {
		big = b
		small = a
	}
	comparisonIndex := len(small) - 1
	return small[comparisonIndex] == big[comparisonIndex]

}

func (s *Server) findCandidates(node *Node) []*Node {
	var candidates []*Node
	for _, dep := range node.dependencies {
		depNode, ok := s.commitPointerMap[dep]
		if !ok {
			fmt.Println(node.key, node.dependencies)
			panic("dependency not in map!")
		}
		if !depNode.flattened {
			fmt.Println(node.key, node.dependencies)
			if _, ok := s.commitPointerMap[dep]; ok {
				fmt.Println("dependency is present in system", depNode.key, depNode.commit)
			}

			panic("dependency is not yet flattened!")
		}
		candidates = append(candidates, depNode)
	}
	s.tm.RLock()
	t, ok := s.txnIndex[node.tId]
	if !ok {
		panic("txn does not exist!")
	}
	for _, write := range t.writeSet {
		if write.flattened {
			candidates = append(candidates, write)
		}
	}
	s.tm.RUnlock()
	return candidates
}

func (s *Server) checkCommonLineage(node *Node) bool {
	// collect candidate nodes
	candidates := s.findCandidates(node)
	// fmt.Printf("\n")
	// s.tm.RLock()
	// fmt.Println("txn writeset", node.tId)
	// for _, node := range s.txnIndex[node.tId].writeSet {
	// 	fmt.Printf("%s ", node.key)
	// }
	// s.tm.RUnlock()
	// fmt.Printf("\n")

	candidateCount := len(candidates)
	for i := range candidates {
		if i != candidateCount-1 {
			if !s.compareLineage(candidates[i].lineage, candidates[i+1].lineage) {
				// fmt.Println("lineage returned false for", candidates[i].lineage, candidates[i+1].lineage)
				return false
			}

		}
	}

	return true
}

func (s *Server) findAppropriateBranch(node *Node) *BranchElement {
	candidates := s.findCandidates(node)

	longest := -1
	var latestCandidate *Node
	for _, candidate := range candidates {
		if len(candidate.lineage) > longest {
			longest = len(candidate.lineage)
			latestCandidate = candidate
		}
	}

	for _, branch := range s.branches {
		if s.compareLineage(latestCandidate.lineage, branch.leaf.lineage) {
			// this is our branch!
			return branch
		}
	}
	return nil

}

func (s *Server) sortBranches() {
	sort.Slice(s.branches[:], func(i, j int) bool {
		return s.branches[i].length < s.branches[j].length
	})

}

func (s *Server) createBranch(branchPoint *Node) *BranchElement {
	b := &BranchElement{
		leaf:   branchPoint,
		length: branchPoint.depth,
	}
	s.branches = append(s.branches, b)
	s.sortBranches()

	return b
}

func (s *Server) addNodetoBranch(node *Node, branch *BranchElement) {
	node.parent = branch.leaf
	branch.leaf.flattenedChildren = append(branch.leaf.flattenedChildren, node)
	node.depth = branch.leaf.depth + 1
	node.lineage = append(node.lineage, branch.leaf.lineage...)
	node.flattened = true

	branch.leaf = node
	branch.length++

}

func (s *Server) correctBranchId(branchPoint *Node, counter uint32) {
	fixIndex := len(branchPoint.lineage) // - 1

	// Search and fix
	queue := []*Node{}
	queue = append(queue, branchPoint.flattenedChildren...)

	for {
		if len(queue) == 0 {
			break
		}
		currNode := queue[0]
		queue = queue[1:]

		// fix this node's branch ID
		if len(currNode.lineage) == len(branchPoint.lineage) {
			currNode.lineage = append(currNode.lineage, counter)
		} else {
			currNode.lineage = append(currNode.lineage, 0)
			copy(currNode.lineage[fixIndex+1:], currNode.lineage[fixIndex:])
			currNode.lineage[fixIndex] = counter
		}

		// traverse its children!
		queue = append(queue, currNode.flattenedChildren...)
	}

}

func (s *Server) determineConflict(node *Node, branch *BranchElement) (bool, *Node) {
	keys := []string{node.key}
	keyMap := make(map[string]*Node)
	for _, dep := range node.dependencies {
		depNode := s.commitPointerMap[dep]
		keys = append(keys, depNode.key)
		keyMap[depNode.key] = depNode
	}

	conflict := false
	var conflictPoint *Node = nil
	for _, key := range keys {
		s.km.RLock()
		fmt.Println("determineConflict key:", key)
		for _, version := range s.keyIndex[key] {
			if version.flattened && version.depth > keyMap[key].depth && s.compareLineage(branch.leaf.lineage, version.lineage) {
				conflict = true
				conflictPoint = version.parent

			}
		}
		s.km.RUnlock()
	}
	return conflict, conflictPoint

}

func (s *Server) determineAbort(node *Node, branchPoint *Node) bool {
	candidates := s.findCandidates(node)

	for _, candidate := range candidates {
		if candidate.depth > branchPoint.depth {
			return true
		}
	}

	return false
}

func (s *Server) flatten(graph []*Node) {
	fmt.Println("reached flatten")

	var count int = 0
	for _, node := range graph {
		// fmt.Println("flattening node", node.key)
		// fmt.Println(count)
		if node.flattened {
			continue
		}
		count++

		if s.checkAbort(node) || !s.checkCommonLineage(node) {
			// abort the current node as well
			// fmt.Println("aborting node1", node.key)
			s.abort(node.tId)
			s.cascadingAbort(node.tId)
			continue
		}

		branch := s.findAppropriateBranch(node)
		if branch == nil {
			// fmt.Println("aborting node2", node.key)
			s.cascadingAbort(node.tId)
			continue
		}

		conflict, conflictPoint := s.determineConflict(node, branch)

		if conflict {
			if s.determineAbort(node, conflictPoint) {
				// println("REACHED ABORT DUE TO BRANCHING")
				s.abort(node.tId)
				atomic.AddUint32(&s.wwAbort, 1)
				s.cascadingAbort(node.tId)
				continue
			}

			s.counter++
			s.correctBranchId(conflictPoint, s.counter)

			b := s.createBranch(conflictPoint)
			s.addNodetoBranch(node, b)
			s.counter++
			node.lineage = append(node.lineage, s.counter)
		} else {
			s.addNodetoBranch(node, branch)
			s.sortBranches()

		}

	}
	fmt.Println("Abort count:", s.abortCount, "Branches:", len(s.branches))

}
func (s *Server) Done(ctx context.Context, req *db.Empty) (result *db.Empty, err error) {
	for {
		if len(s.snapshot) < s.epochSize {
			break
		}
	}
	// time.Sleep(5 * time.Second)
	fmt.Println("number of aborted reads:", s.abortedReads)
	fmt.Println("remaining node size: ", len(s.snapshot))

	s.finish = true
	s.tm.RLock()
	c := 0
	for _, txn := range s.txnIndex {
		if txn.abort {
			c++
		}
	}

	fmt.Println(c, "txns aborted out of", len(s.txnIndex))
	fmt.Println(s.abortedReads, "reads aborted out of", s.totalReads, "reads")
	fmt.Println("client commits", s.clientCommits, "client aborts", s.clientAborts, "ww conflicts", s.wwAbort, "cascading aborts", s.cascadingAborts)
	s.tm.RUnlock()
	return &db.Empty{}, nil
}

func (s *Server) Print(ctx context.Context, req *db.Empty) (result *db.Empty, err error) {
	s.km.RLock()
	s.tm.RLock()
	defer s.km.RUnlock()
	defer s.tm.RUnlock()
	println("Keys:")
	for key, vList := range s.keyIndex {
		fmt.Printf("Key %s\n", key)
		for _, node := range vList {
			var parentCommit uint64
			if node.parent != nil {
				parentCommit = node.parent.commit
			} else {
				parentCommit = 9999
			}
			fmt.Printf("Value: %s Lineage: %v tId: %v Commit: %v Parent: %v\n", node.value, node.lineage, node.tId, node.commit, parentCommit)
		}
	}

	println("Transactions:")
	for txn, tData := range s.txnIndex {
		fmt.Printf("Txn %d Abort:  %v\n", txn, tData.abort)
	}
	return &db.Empty{}, nil
}

func (s *Server) Disseminate(ctx context.Context, req *db.DisseminateRequestList) (*db.Empty, error) {
	if s.stage != FLATTEN {
		respList := make([]NodeMetadata, len(req.Metadata))
		for i := range req.Metadata {
			n := NodeMetadata{
				Commit:       req.Metadata[i].Commit,
				Dependencies: req.Metadata[i].Dependencies,
				Key:          req.Metadata[i].Key,
				Tid:          req.Metadata[i].Tid,
			}
			respList[i] = n
		}
		// TODO: Add a lock here cause there can be concurrent inserts.
		s.gSm.Lock()
		s.globalSnapshot = append(s.globalSnapshot, respList...)
		s.gSm.Unlock()
		atomic.AddUint32(&s.recvCount, 1)
		if atomic.CompareAndSwapUint32(&s.stage, COLLECT, COLLECT) { // || atomic.CompareAndSwapUint32(&s.stage, WAIT, COLLECT) {
			go s.StateMachine()
		}
	}
	return &db.Empty{}, nil
}

func (s *Server) MissingParents(ctx context.Context, req *db.DisseminateRequestList) (*db.Empty, error) {
	return &db.Empty{}, nil
}

func (s *Server) Notify(ctx context.Context, req *db.ACK) (*db.Empty, error) {
	if int(s.ackCount) == len(s.peers)-1 {
		return &db.Empty{}, common.ErrExcessAck
	}

	atomic.AddUint32(&s.ackCount, 1)

	return &db.Empty{}, nil

}
