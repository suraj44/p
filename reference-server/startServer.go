package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"

	_ "net/http/pprof"

	"github.com/suraj44/graph-kv/db/db"
	"google.golang.org/grpc"
)

// var epochSize = flag.Uint64("epochSize", 10000, "size of an epoch for tree flattening")
var serverAddress = flag.String("address", "10.0.3.5:5000", "address of the server")
var peers = flag.String("peers", "", "comma separated list of peer nodes")
var epochSize = flag.Int("epochSize", 1000, "size of an epoch")
var errChan chan error
var stopChan chan os.Signal

// var peers = flag.String("peers", "", "comma separated list of peer nodes")

func startServer(server *Server, addr string, errChan chan error, stopChan chan os.Signal) {
	// lis, err := server.Init(*serverAddress, clusterAddrMap, *shardCount, 1)
	// if err != nil {
	// 	log.Fatalf("failed to listen: %v", err)
	// }
	signal.Notify(stopChan, syscall.SIGTERM, syscall.SIGINT)

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
		return
	}

	grpcServer := grpc.NewServer()
	db.RegisterDbServiceServer(grpcServer, server)

	go func() {
		if err := grpcServer.Serve(lis); err != nil {
			errChan <- err
		}
	}()

	log.Println("Server listen at", addr)

	defer func() {
		grpcServer.GracefulStop()
	}()

	select {
	case err := <-errChan:
		log.Printf("Fatal error: %v\n", err)
	case <-stopChan:
		fmt.Printf("reached stopChan")

	}

}

func main() {
	flag.Parse()
	// runtime.SetBlockProfileRate(1)

	// if strings.Contains(*serverAddress, "10.0.1.5") {
	// 	go func() {
	// 		http.ListenAndServe("10.0.1.5:3000", nil)
	// 	}()
	// }
	errChan = make(chan error)
	stopChan = make(chan os.Signal)

	peerSlice := strings.Split(*peers, ",")

	server := NewServer(*serverAddress, peerSlice, *epochSize)
	startServer(server, *serverAddress, errChan, stopChan)

}
