package main

import (
	"fmt"
	"runtime"
)

func printTestingInfo() {
	pc := make([]uintptr, 64)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	fmt.Printf("\n== Test Case ==\n\n%s:%d %s\n", frame.File, frame.Line, frame.Function)
}

// func TestSingleBranch(t *testing.T) {
// 	// figure out client stuff
// 	printTestingInfo()
// 	serverAddress := "127.0.0.1:5000"
// 	epochSize := 2
// 	s := NewServer(serverAddress, epochSize)
// 	go startServer(s, serverAddress)

// 	c, err := client.NewClient([]string{serverAddress})

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	cA, _ := c.Set("a", "0", 0, []uint64{0})
// 	resp, _ := c.GetbyKey("a")

// 	if resp.Value != "0" {
// 		t.Errorf("want %s got %s", "b", resp.Value)
// 	}
// 	c.Set("b", "c", 1, []uint64{0})
// 	c.Set("a", "1", 2, []uint64{cA.Commit})
// 	// time.Sleep(3 * time.Second)
// 	c.Set("c", "d", 3, []uint64{0})
// 	c.Set("a", "2", 4, []uint64{cA.Commit})

// 	if len(s.branches) != 2 {
// 		t.Errorf("wanted %d branches but got %d", 1, len(s.branches))
// 	}
// 	if s.branches[0].leaf.key != "a" {
// 		t.Errorf("node did not become leaf. expected %s but got %s\n", "a", s.branches[0].leaf.key)
// 	}
// }

// func TestMultipleBranches(t *testing.T) {
// 	printTestingInfo()
// 	serverAddress := "127.0.0.1:5000"
// 	epochSize := 2
// 	s := NewServer(serverAddress, epochSize)
// 	go startServer(s, serverAddress)

// 	c, err := client.NewClient([]string{serverAddress})

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	cA, _ := c.Set("a", "0", 0, []uint64{0})
// 	c.Set("b", "0", 0, []uint64{0})
// 	cC, _ := c.Set("c", "0", 0, []uint64{0})

// 	cC1, _ := c.Set("c", "1", 1, []uint64{cC.Commit})
// 	cC2, _ := c.Set("c", "2", 2, []uint64{cA.Commit, cC.Commit})

// 	c.Set("d", "0", 3, []uint64{cC1.Commit})
// 	c.Set("e", "0", 4, []uint64{cC2.Commit})
// 	time.Sleep(time.Second)

// 	if len(s.branches) != 2 {
// 		t.Errorf("expected 2 branches but got %d", len(s.branches))
// 	}

// }

// func TestBranchIDCorrection(t *testing.T) {
// 	printTestingInfo()
// 	serverAddresses := []string{"127.0.0.1:5000", "127.0.0.1:5001"}
// 	epochSize := 2
// 	s := NewServer(serverAddresses[0], serverAddresses, epochSize)
// 	s1 := NewServer(serverAddresses[1], serverAddresses, epochSize)
// 	fmt.Println("inital len of snapshot is", len(s.snapshot))
// 	errChan = make(chan error)
// 	stopChan = make(chan os.Signal)

// 	go startServer(s, serverAddresses[0], errChan, stopChan)
// 	go startServer(s1, serverAddresses[1], errChan, stopChan)
// 	time.Sleep(200 * time.Millisecond)

// 	c, err := client.NewClient(serverAddresses)

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	// cA, err := c.Set("a", "0", 1, []uint64{0})
// 	// fmt.Println(err)
// 	// fmt.Println(cA)
// 	txn := c.NewClientTxn()
// 	cA, _ := txn.Write("a", "0", []uint64{0})
// 	txn.Write("a", "1", []uint64{cA})
// 	txn.Write("c", "0", []uint64{0})
// 	txn.Commit()
// 	// time.Sleep(5 * time.Second)

// 	txn1 := c.NewClientTxn()
// 	txn1.Write("b", "0", []uint64{cA})
// 	time.Sleep(1000 * time.Millisecond)
// 	txn1.Write("d", "0", []uint64{cA})
// 	txn1.Commit()

// 	// c.Set("b", "0", 2, []uint64{cA.Commit})

// 	// c.Set("d", "0", 2, []uint64{cA.Commit})
// 	// // c.Set("e", "0", 1, []uint64{0})
// 	// // c.Set("f", "0", 1, []uint64{0})
// 	// c.CommitTxn(2)
// 	time.Sleep(time.Second)

// 	expectedBranchIds := [][]uint32{{0, 1}, {0, 2}}
// 	for _, branch := range s.branches {
// 		found := false
// 		for _, bId := range expectedBranchIds {
// 			if reflect.DeepEqual(branch.leaf.lineage, bId) {
// 				found = true
// 				break
// 			}
// 		}
// 		if !found {
// 			t.Errorf("branch ID %v was not expected in the tree", branch.leaf.lineage)
// 		}
// 	}

// 	c.Print()
// 	stopChan <- syscall.SIGTERM
// 	stopChan <- syscall.SIGTERM

// }

// func TestCorrectionForMultiplePaths(t *testing.T) {
// 	printTestingInfo()
// 	serverAddress := "127.0.0.1:5000"
// 	epochSize := 2
// 	s := NewServer(serverAddress, epochSize)
// 	go startServer(s, serverAddress)

// 	c, err := client.NewClient([]string{serverAddress})

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	aC, _ := c.Set("a", "0", 0, []uint64{0})
// 	bC, _ := c.Set("b", "0", 0, []uint64{0})
// 	c.Set("a", "1", 0, []uint64{0})
// 	c.Set("b", "1", 0, []uint64{0})
// 	c.Set("g", "1", 0, []uint64{0})
// 	c.Set("e", "0", 1, []uint64{bC.Commit})
// 	c.Set("f", "0", 2, []uint64{aC.Commit})

// 	time.Sleep(100 * time.Millisecond)

// 	if len(s.branches) != 3 {
// 		t.Errorf("expected %d branches but got %d", 3, len(s.branches))
// 	}

// 	expectedBranchIds := [][]uint32{{0, 3, 1}, {0, 3, 2}, {0, 4}}
// 	for _, branch := range s.branches {
// 		found := false
// 		for _, bId := range expectedBranchIds {
// 			if reflect.DeepEqual(branch.leaf.lineage, bId) {
// 				found = true
// 				break
// 			}
// 		}
// 		if !found {
// 			t.Errorf("expected branch id %v but it is not present", branch.leaf.lineage)
// 		}
// 	}

// 	// c.Print()
// }

// func TestTxnAbort1(t *testing.T) {
// 	printTestingInfo()
// 	serverAddresses := []string{"127.0.0.1:5000"}
// 	epochSize := 2
// 	errChan = make(chan error)
// 	stopChan = make(chan os.Signal)
// 	s := NewServer(serverAddresses[0], serverAddresses, epochSize)
// 	startServer(s, serverAddresses[0], errChan, stopChan)

// 	time.Sleep(200 * time.Millisecond)

// 	c, err := client.NewClient(serverAddresses)

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	aC, _ := c.Set("a", "0", 0, []uint64{0})
// 	c.Set("b", "0", 0, []uint64{0})
// 	c.Set("a", "1", 1, []uint64{0})
// 	c.Set("c", "1", 1, []uint64{0})

// 	c.Set("b", "1", 1, []uint64{aC.Commit}) // txn 1 should abort!

// 	if s.txnIndex[1].abort != true {
// 		t.Errorf("Expected txn 1 to abort but it committed")
// 	}
// 	c.Print()
// }

// func TestTxnAbort2(t *testing.T) {
// 	printTestingInfo()
// 	serverAddresses := []string{"127.0.0.1:5000"}
// 	epochSize := 1
// 	errChan = make(chan error)
// 	stopChan = make(chan os.Signal)

// 	s := NewServer(serverAddresses[0], serverAddresses, epochSize)
// 	go startServer(s, serverAddresses[0], errChan, stopChan)

// 	time.Sleep(200 * time.Millisecond)

// 	c, err := client.NewClient(serverAddresses)

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	txn := c.NewClientTxn()
// 	aC, _ := txn.Write("a", "0", []uint64{0})
// 	txn.Write("b", "0", []uint64{0})
// 	txn.Commit()
// 	aC, err := c.Set("a", "0", 0, []uint64{0})
// 	if err != nil {
// 		t.Errorf("error with set(): %s", err)
// 	}
// 	c.Set("b", "0", 0, []uint64{0})
// 	c.Set("a", "1", 1, []uint64{0})
// 	cC, err := c.Set("c", "1", 2, []uint64{0})
// 	if err != nil {
// 		t.Errorf("error with set(): %s", err)
// 	}

// 	fmt.Println(aC, cC)
// 	c.Set("b", "1", 3, []uint64{aC, cC}) // txn 1 should abort!

// 	time.Sleep(2000 * time.Millisecond)

// 	c.Print()
// 	stopChan <- syscall.SIGTERM
// }

// func TestGetFunctionality(t *testing.T) {
// 	printTestingInfo()
// 	serverAddress := "127.0.0.1:5000"
// 	epochSize := 2
// 	s := NewServer(serverAddress, epochSize)
// 	go startServer(s, serverAddress)

// 	c, err := client.NewClient([]string{serverAddress})

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	c.Set("a", "0", 0, []uint64{0})
// 	c.Set("a", "1", 0, []uint64{0})

// 	want := "0"
// 	got, err := c.GetbyKey("a")
// 	if err != nil {
// 		t.Errorf("wanted nil but got error: %s", err.Error())
// 	}

// 	if got.Value != want {
// 		t.Errorf("wanted vlue %s but got %s", want, got.Value)
// 	}
// }

// func TestGetAbortedWriteGet(t *testing.T) {
// 	printTestingInfo()
// 	serverAddress := "127.0.0.1:5000"
// 	epochSize := 2
// 	s := NewServer(serverAddress, epochSize)
// 	go startServer(s, serverAddress)

// 	c, err := client.NewClient([]string{serverAddress})

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	c.Set("a", "0", 0, []uint64{0})
// 	c.Set("a", "1", 1, []uint64{0})

// 	s.tm.Lock()
// 	s.txnIndex[0].abort = true
// 	s.tm.Unlock()

// 	want := "1"
// 	got, err := c.GetbyKey("a")

// 	if err != nil {
// 		t.Errorf("wanted nil but got error: %s", err.Error())
// 	}

// 	if got.Value != want {
// 		t.Errorf("wanted vlue %s but got %s", want, got.Value)
// 	}
// }

// func TestGetAllVersionsAborted(t *testing.T) {
// 	printTestingInfo()
// 	serverAddress := "127.0.0.1:5000"
// 	epochSize := 2
// 	s := NewServer(serverAddress, epochSize)
// 	go startServer(s, serverAddress)

// 	c, err := client.NewClient([]string{serverAddress})

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}

// 	c.Set("a", "0", 0, []uint64{0})
// 	c.Set("a", "1", 1, []uint64{0})

// 	s.tm.Lock()
// 	s.txnIndex[0].abort = true
// 	s.txnIndex[1].abort = true
// 	s.tm.Unlock()

// 	got, err := c.GetbyKey("a")

// 	if err != nil {
// 		t.Errorf("wanted nil but got error: %s", err.Error())
// 	}

// 	if !reflect.DeepEqual(got.Error, string(common.ErrNoValue)) {
// 		t.Errorf("expected ErrNoValue error but got %s", got.Error)
// 	}
// }

// func TestClientCommit(t *testing.T) {
// 	printTestingInfo()
// 	serverAddress := "127.0.0.1:5000"
// 	epochSize := 2
// 	s := NewServer(serverAddress, epochSize)
// 	go startServer(s, serverAddress)

// 	c, err := client.NewClient([]string{serverAddress})

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}
// 	txn := c.NewClientTxn()
// 	cA, _ := txn.Write("a", "0", []uint64{0})
// 	txn.Commit()

// 	s.cm.RLock()
// 	commit := s.commitIndex[cA].flattened
// 	s.cm.RUnlock()

// 	if !commit {
// 		t.Errorf("expected txn to commit and flattten but it did not.")
// 	}
// }

// func TestClientAbort(t *testing.T) {
// 	printTestingInfo()
// 	serverAddress := "127.0.0.1:5000"
// 	epochSize := 2
// 	s := NewServer(serverAddress, epochSize)
// 	go startServer(s, serverAddress)

// 	c, err := client.NewClient([]string{serverAddress})

// 	if err != nil {
// 		t.Errorf("error creating client: %s", err)
// 	}
// 	txn := c.NewClientTxn()
// 	cA, _ := txn.Write("a", "0", []uint64{0})
// 	txn.Abort()

// 	s.cm.RLock()
// 	commit := s.commitIndex[cA].flattened
// 	s.cm.RUnlock()

// 	if commit {
// 		t.Errorf("expected txn to abort but write was flattened")
// 	}

// 	if !s.checkTxnAbort(txn.Tid()) {
// 		t.Errorf("expected txn to abort but abort is marked 'false'")
// 	}

// }
