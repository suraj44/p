#!/usr/bin/env bash

workload=$1
shift
uNum=$1 #10000
tNum=$2 #1000
length=$3 #20
warmup=$4 #2
cooldown=$5 #2
threads=$6 #80
serverList=$7 #"10.0.3.4:9000"
output=$8 #$1
#YCSB configurations
zipf=$9; shift
keyNum=$9; shift
keyLen=$9; shift
valLen=$9; shift
opN=$9; shift
readP=$9; shift
writeP=$9; shift
rmwP=$9; shift
roTxnP=$9; shift
rwTxnP=$9; shift
cTxnP=$9; shift
cTxnType=$9

#cd ~/graph-kv/workload/example; ./example -skipPrefill  -t $threads -uNum $uNum -tNum $tNum -length $length -warmup $warmup -cooldown $cooldown -output out/$output.txt -peers $serverList; exit

#execDir="~/graph-kv/workload/example"
execDir="`dirname $0`"
execDir="`cd $execDir; pwd`"
if [ "$workload" == "retwis" ]; then
  cd $execDir; ./example -skipPrefill  -t $threads \
    -workload $workload \
    -uNum $uNum \
    -tNum $tNum -length $length -warmup $warmup -cooldown $cooldown -output out/$output.txt -peers $serverList; exit
elif [ "$workload" == "ycsb" ]; then
  cd $execDir; ./example -skipPrefill  -t $threads \
    -workload $workload \
    -zipf $zipf -keyNum $keyNum -keyLen $keyLen -valLen $valLen \
    -opN $opN -readP $readP -writeP $writeP -rmwP $rmwP \
    -roTxnP $roTxnP -rwTxnP $rwTxnP -cTxnP $cTxnP -cTxnType $cTxnType \
    -tNum $tNum -length $length -warmup $warmup -cooldown $cooldown -output out/$output.txt -peers $serverList; exit
fi
