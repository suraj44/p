#!/usr/bin/env bash

nClient=3
uNum=10000
tNum=1000
length=50
warmup=5
cooldown=5
threads=5
serverAddress="10.0.3.4:9000"
hosts=("blue12" "blue11" "blue15" "red13" "red14")
finalOutput="final.csv"
workingThreads=(2 5 10 20 40 60)
waitPids=()

# TODO: delete all existing txt files from output directory
for file in out/*.txt; do
    rm file
done

# Delete old and create new output CSV
if [[ -e "$finalOutput" ]]
then
    rm "$finalOutput"
fi
touch "$finalOutput"

echo "nClients,workingThreads,throughput,latency" > $finalOutput

# Start server at designated server address
pussh -h blue04 ~/graph-kv/rpc-server/rpc-server -address 10.0.3.4:9000 -epoch 30000 & # how do we separate the logic for "waiting" for this process? We need to kill this process at the end of the experiment.
sleep 2

# Prefill the storage system with the metadata required for the given users
./example -address $serverAddress -uNum $uNum -prefillOnly true 

# Spawn the required number of clients and wait until they finish
for ((i = 0 ; i < $nClient ; i++)); do
    pussh -h ${hosts[$i]} ~/graph-kv/workload/example/runc.sh $uNum $tNum $length $warmup $cooldown $threads $serverAddress ${hosts[$i]} &
    waitPids[${i}]=$!
    # (./runclient$i.sh $nClient $uNum $tNum $length $warmup $cooldown $threads $serverAddress $i) &
done

# wait for all pids
for pid in ${waitPids[*]}; do
    wait $pid
done

# kill the server. TODO: change server to a hyperparameter that can be used everywhere.
pussh -h blue04 pkill -f rpc-server

#wait #TODO: how we kill the server process when all the clients are finished? 


# Parse the output txt files generated by the clients and calculate overall throughput and avg latency
throughput=0
latency=0
for file in out/*.txt; do
    echo $file
	tmp=`awk '/throughput=/' $file | awk -F'=' '{printf $2}'`
    throughput=$((throughput+tmp))
    echo tmp
    tmp=`awk '/latency=/' $file | awk -F'=' '{printf $2}'`
    echo tmp
    latency=$((latency+tmp))
done

latency=$((latency/nClient))
echo "total throughput = $throughput"
echo "total latency = $latency"
echo "$nClient,$threads, $throughput,$latency" >> $finalOutput

# Append the metrics to the final CSV



# for ((i = 0 ; i < $nClient + 1 ; i++)); do
#     ./example -t $threads -uNum $uNum -tNum $tNum -length $length -warmup $warmup -cooldown $cooldown -output tmp$i.txt -address 127.0.0.1:5000 &
# done

# wait