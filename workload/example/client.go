package main

import (
	"github.com/suraj44/graph-kv/client"
	"github.com/suraj44/graph-kv/workload"
)

type ClientDriver struct {
	client *client.Client
}

func NewClientDriver(client *client.Client) workload.Driver {
	c := &ClientDriver{
		client: client,
	}
	return c
}

func (c *ClientDriver) BeginTxn() workload.Txn {
	return c.client.NewClientTxn()
}
