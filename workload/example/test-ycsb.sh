#!/usr/bin/env bash

#Workload Types: retwis or ycsb
#workload="retwis"
workload="ycsb"

nClient=1
nServer=1
tNum=0
uNum=10000
length=20
warmup=3
cooldown=3
#threads=15
threads=10
epochSize=30000
serverAddress="10.0.3.4:9000"
#servers=( "10.0.1.5" "10.0.1.8" "10.0.1.9" "10.0.3.5")
servers=("10.0.2.13")
portNumber=5000
clients=("10.0.2.14")
#clients=("red04" "blue05")
finalOutput="final-shard.csv"
workingThreads=(2 5 10 20 40 60)
waitPids=()
#YCSB configurations
#zipf="-1"
zipf="0.99"
keyNum="10"
keyLen="8"
valLen="8"
opN=6
readP=0
writeP=0
rmwP=100
roTxnP=25
#rwTxnP=75
rwTxnP=0
cTxnP=75
cTxnType="halfrw"

curDir="`dirname $0`"
curDir="`cd $curDir; pwd`"
echo $curDir

for file in out/*.txt; do
    if [[ -e "$file" ]]
    then
        rm "$file"
    fi
done

# Delete old and create new output CSV
if [[ -e "$finalOutput" ]]
then
    rm "$finalOutput"
fi
touch "$finalOutput"


echo "nShards,nClients,workingThreads,throughput,latency" > $finalOutput

peerList=${servers[0]}:$portNumber
for ((i = 1 ; i < $nServer ; i++)); do
    peerList+=",${servers[$i]}:$portNumber"
done
echo $peerList
# Start all shards/servers
for ((i = 0 ; i < $nServer ; i++)); do
    #pussh -h ${servers[$i]} ~/graph-kv/server/server -address ${servers[$i]}:$portNumber -epochSize $epochSize -peers $peerList  &
    pussh -h ${servers[$i]} $curDir/../../reference-server/reference-server -address ${servers[$i]}:$portNumber -epochSize $epochSize  &
done
sleep 6


test=$(printf "%s:%s" "${servers[0]}"  "$portNumber")
echo $test

echo "$workload"
# prefill
#./example -peers $peerList  -uNum $uNum -prefillOnly t
if [ "$workload" == "retwis" ]; then
  ./example -peers $peerList  -workload $workload -uNum $uNum -prefillOnly t
elif [ "$workload" == "ycsb" ]; then
  ./example -peers $peerList  -workload $workload \
    -zipf $zipf -keyNum $keyNum \
    -keyLen $keyLen -valLen $valLen \
    -opN $opN -readP $readP -writeP $writeP -rmwP $rmwP \
    -roTxnP $roTxnP -rwTxnP $rwTxnP -cTxnP $cTxnP -cTxnType $cTxnType \
    -prefillOnly t
fi

# echo "prefilling done!"

# run clients
for ((i = 0 ; i < $nClient ; i++)); do
    serverIndex=$(shuf -i 0-0 -n 1)
    #pussh -h ${clients[$i]} ~/graph-kv/workload/example/runc.sh $uNum $tNum $length $warmup $cooldown $threads $peerList ${clients[$i]} &
    pussh -h ${clients[$i]} $curDir/runc.sh $workload \
      $uNum $tNum $length $warmup $cooldown $threads $peerList ${clients[$i]}\
      $zipf $keyNum $keyLen $valLen \
      $opN $readP $writeP $rmwP $roTxnP $rwTxnP $cTxnP $cTxnType \
      &
    waitPids[${i}]=$!
done

# wait for all pids
for pid in ${waitPids[*]}; do
    wait $pid
done


# kill all servers
for ((i = 0 ; i < $nServer ; i++)); do
    pussh -h ${servers[$i]} pkill -f reference-server
done

# parse outputs and aggregate stats
throughput=0
completed_throughput=0
latency=0
for file in out/*.txt; do
    if [[ -e "$file" ]]
    then
        echo $file
        tmp=`awk '/throughput=/' $file | awk -F'=' '{printf $2}'`
        throughput=$((throughput+tmp))
        tmp=`awk '/completed_throughput=/' $file | awk -F'=' '{printf $2}'`
        # echo tmp
        completed_throughput=$((completed_throughput+tmp))
        tmp=`awk '/latency=/' $file | awk -F'=' '{printf $2}'`
        # echo tmp
        latency=$((latency+tmp))
    fi
done

latency=$((latency/nClient))
echo "total throughput = $throughput"
echo "total completed throughput = $completed_throughput"
echo "total latency = $latency"
echo "$nClient,$threads, $throughput,$latency" >> $finalOutput
echo "Done"
