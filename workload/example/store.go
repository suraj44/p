package main

import (
	"github.com/suraj44/graph-kv/store"
	"github.com/suraj44/graph-kv/workload"
)

type StoreDriver struct {
	store *store.Store
}

func NewStoreDriver(store *store.Store) workload.Driver {
	s := &StoreDriver{
		store: store,
	}
	return s
}

func (s *StoreDriver) BeginTxn() workload.Txn {
	return s.store.NewStoreTxn()
}
