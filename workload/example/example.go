package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"runtime/pprof"

	// "github.com/suraj44/graph-kv/store"

	"github.com/suraj44/graph-kv/client"
	"github.com/suraj44/graph-kv/workload"
)

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
var peers = flag.String("peers", "", "comma separated list of peer nodes")

var numWorkers = flag.Int("t", 1, "Number of concurren workers")
var tNum = flag.Int("tNum", 5, "Number of txns/worker")

var experimentLength = flag.Int("length", 60, "Desired length of closed loop experiment in seconds")
var warmUp = flag.Int("warmup", 3, "warm up time")
var coolDown = flag.Int("cooldown", 3, "cool down time")
var serverAddress = flag.String("address", "127.0.0.1:5000", "IP address and port number of server")
var outputFile = flag.String("output", "output.txt", "Name of file with output log")
var skipPrefill = flag.Bool("skipPrefill", false, "whether to prefill the storage system with users or not")
var prefillOnly = flag.Bool("prefillOnly", false, "whether to only prefill the storage system with users and their metadata")

// Workload Type
var workloadType = flag.String("workload", "retwis", "worklads: retwis, ycsb")

// Retwis configurations
var uNum = flag.Int("uNum", 10, "Number of users for Retwis")
var followOp = flag.Int("follow", 10, "Percentage of follow operations")
var postOp = flag.Int("post", 30, "Percentage of post operations")
var loadOp = flag.Int("load", 60, "Percentage of load timeline operations")

// YCSB configurations
var zipf = flag.Float64("zipf", -1, "Zipfian alpha. negative for uniform distribution")
var keyNum = flag.Int("keyNum", 10, "number of keys")
var keyLen = flag.Int("keyLen", 8, "key length (in bytes)")
var valLen = flag.Int("valLen", 8, "value length (in bytes)")
var opN = flag.Int("opN", 6, "number of operations per txn")
var readP = flag.Int("readP", 0, "percentage of reads in read-write txns")
var writeP = flag.Int("writeP", 0, "percentage of writes in read-write txns")
var rmwP = flag.Int("rmwP", 100, "percentage of read-modify-write operations in read-write txns")
var roTxnP = flag.Int("roTxnP", 75, "percentage of read-only txn")
var rwTxnP = flag.Int("rwTxnP", 0, "percentage of read-write txn")
var cTxnP = flag.Int("cTxnP", 25, "percentage of customized txn")
var cTxnType = flag.String("cTxnType", workload.YCSB_HALF_RW_TXN,
	fmt.Sprintf("customized txn type: %s (default) or %s", workload.YCSB_HALF_RW_TXN, workload.YCSB_PARALLEL_RW_TXN))

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	peerSlice := strings.Split(*peers, ",")

	/*
	 * Use either in-memory implementation or client library.
	 * Uncomment whichever one is needed.
	 */
	// store := store.NewStore(5000)
	// driver := NewStoreDriver(store)
	client, err := client.NewClient(peerSlice)
	if err != nil {
		fmt.Println(err)
		return
	}
	driver := NewClientDriver(client)

	// Configures workloads
	workload.SetRandomSeed(time.Now().UnixNano()) // Rand seed
	var r workload.Workload
	switch *workloadType {
	case "retwis":
		r = workload.NewRetwis(driver, ":", 50, *uNum, 0, *followOp, *postOp, *loadOp)
	case "ycsb":
		r = workload.NewYcsb(driver, *zipf, *keyNum, *keyLen, *valLen, *opN, *readP, *writeP, *rmwP, *roTxnP, *rwTxnP, *cTxnP, *cTxnType)
	default:
		panic(errors.New("Invalid workload type: " + *workloadType))
	}

	fmt.Println("\n=== Random Test Transactions ===")
	fmt.Println("workloadType=", *workloadType)
	fmt.Println("uNum=", *uNum)
	fmt.Println("tNum=", *tNum)
	fmt.Println("numWorkers=", *numWorkers)
	fmt.Println("followOp=", *followOp)
	fmt.Println("postOp=", *postOp)
	fmt.Println("loadOp=", *loadOp)
	fmt.Println("experimentLength=", *experimentLength)
	fmt.Println("warmUp=", *warmUp)
	fmt.Println("coolDown=", *coolDown)
	fmt.Println("skipPrefill=", *skipPrefill)
	fmt.Println("serverAddress=", *serverAddress)
	fmt.Println("outputFile=", *outputFile)
	// YCSB configurations
	fmt.Println("zipf=", *zipf)
	fmt.Println("keyNum=", *keyNum)
	fmt.Println("keyLen=", *keyLen)
	fmt.Println("valLen=", *valLen)
	fmt.Println("opN=", *opN)
	fmt.Println("readP=", *readP)
	fmt.Println("writeP=", *writeP)
	fmt.Println("rmwP=", *rmwP)
	fmt.Println("roTxnP=", *roTxnP)
	fmt.Println("rwTxnP=", *rwTxnP)
	fmt.Println("cTxnP=", *cTxnP)

	if *prefillOnly {
		fmt.Println("prefilling....")
		r.LoadDB()
		fmt.Println("done with prefill!")
		return
	}

	if !*skipPrefill {
		fmt.Println("prefilling....")
		r.LoadDB()
		fmt.Println("done with prefill!")
	}

	shardedCounter := make([]uint64, *numWorkers)
	latencyCounter := make([]time.Duration, *numWorkers)
	steadyState := *experimentLength - *warmUp - *coolDown
	fmt.Println("steady state", steadyState)
	for i := range shardedCounter {
		shardedCounter[i] = 0
		latencyCounter[i] = 0
	}
	fmt.Println("Start")
	var wg sync.WaitGroup

	/*
	 * There are 4 states in this experiment.
	 * State 0 - Initialize go routines
	 * State 1 - Warm up
	 * State 2 - Steady State
	 * State 3 - Cool down
	 */

	state := 0
	for w := 0; w < *numWorkers; w++ { // Creates two workers, each executes a transaction at once
		wg.Add(1)
		go func(wId int) {
			rand.Seed(time.Now().UTC().UnixNano())
			for {
				if state == 1 {
					break
				}
			}

			// Each worker executes 5 random transactions
			// for i := 0; i < *tNum; i++ {
			for {
				// Random txn type, excluding AddUser
				if state == 4 {
					break
				}

				t := r.GenTxn()
				startTxn := time.Now()
				t.Exec()

				if state == 2 {
					shardedCounter[wId]++
					latencyCounter[wId] += time.Since(startTxn)
				}

			}

			wg.Done()
		}(w)
	}
	state = 1
	time.Sleep(time.Duration(*warmUp) * time.Second)
	start := time.Now()
	state = 2
	time.Sleep(time.Duration(steadyState) * time.Second)
	elapsed := time.Since(start)
	state = 3
	time.Sleep(time.Duration(*coolDown) * time.Second)
	state = 4
	wg.Wait() // waits for all workers to complete

	fmt.Println("reached done")
	client.Done()
	flattenTime := time.Since(start)

	elapsedTime := uint64(elapsed / time.Microsecond)
	flattenTimeV2 := uint64(flattenTime / time.Microsecond)

	totalCount := uint64(0)
	totalLatency := time.Duration(0)
	for i := range shardedCounter {
		totalCount += shardedCounter[i]
		totalLatency += latencyCounter[i]
	}

	fmt.Printf("%d txns completed in %s\n", totalCount, elapsed)
	fmt.Printf("throughput=%d\n", (totalCount*uint64(time.Second/time.Microsecond))/elapsedTime)
	fmt.Printf("completed_throughput=%d\n", (totalCount*uint64(time.Second/time.Microsecond))/uint64(flattenTimeV2))
	fmt.Printf("latency=%d\n", uint64(totalLatency/time.Duration(totalCount)))

	stats := fmt.Sprintf("throughput=%d\nlatency=%d\n", (totalCount*uint64(time.Second/time.Microsecond))/elapsedTime, totalLatency/time.Duration(totalCount))
	f, err := os.Create(*outputFile)

	check(err)

	defer f.Close()
	f.Write([]byte(stats))
}

// Helper functions
func str(i int) string {
	return strconv.Itoa(i)
}
