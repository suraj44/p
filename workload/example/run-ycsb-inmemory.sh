#!/usr/bin/env bash

#Workload Types: retwis or ycsb
#workload="retwis"
workload="ycsb"

nClient=1
nServer=1
tNum=0
uNum=10000
length=20
warmup=3
cooldown=3
threads=15
epochSize=5000
serverAddress="10.0.3.4:9000"
#servers=( "10.0.1.5" "10.0.1.8" "10.0.1.9" "10.0.3.5")
servers=("10.0.1.5")
portNumber=5000
clients=("red04" "blue05" "red06" "red14" "red13")
#clients=("red04" "blue05")
finalOutput="final-shard.csv"
workingThreads=(2 5 10 20 40 60)
waitPids=()
#YCSB configurations
zipf="-1"
keyNum="1000"
keyLen="8"
valLen="8"
opN=6
readP=0
writeP=0
rmwP=100
roTxnP=50
rwTxnP=0
cTxnP=50
cTxnType="halfrw"

curDir="`dirname $0`"
curDir="`cd $curDir; pwd`"
echo $curDir

for file in out/*.txt; do
    if [[ -e "$file" ]]
    then
        rm "$file"
    fi
done

# Delete old and create new output CSV
if [[ -e "$finalOutput" ]]
then
    rm "$finalOutput"
fi
touch "$finalOutput"


echo "nShards,nClients,workingThreads,throughput,latency" > $finalOutput

./example -cpuprofile tree.prof -t $threads \
    -workload $workload \
    -zipf $zipf -keyNum $keyNum -keyLen $keyLen -valLen $valLen \
    -opN $opN -readP $readP -writeP $writeP -rmwP $rmwP \
    -roTxnP $roTxnP -rwTxnP $rwTxnP -cTxnP $cTxnP -cTxnType $cTxnType \
    -tNum $tNum -length $length -warmup $warmup -cooldown $cooldown -output out/$output.txt

# parse outputs and aggregate stats
throughput=0
latency=0
for file in out/*.txt; do
    if [[ -e "$file" ]]
    then
        echo $file
        tmp=`awk '/throughput=/' $file | awk -F'=' '{printf $2}'`
        throughput=$((throughput+tmp))
        # echo tmp
        tmp=`awk '/latency=/' $file | awk -F'=' '{printf $2}'`
        # echo tmp
        latency=$((latency+tmp))
    fi
done

latency=$((latency/nClient))
echo "total throughput = $throughput"
echo "total latency = $latency"
echo "$nClient,$threads, $throughput,$latency" >> $finalOutput
echo "Done"
