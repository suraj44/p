package workload

import (
	"log"
	"math/rand"
)

//// YCSB+T
/*
* Data Scheme
* key (string)--> value(string)
 */
type Ycsb struct {
	d Driver

	keyNum int // number of keys
	keyLen int // key length in bytes
	valLen int // value size in bytes
	opN    int // number of operations per txn
	readP  int // percentage of read operations per txn
	writeP int // percentage of write operations per txn
	rmwP   int // percentage of read-modify-write operations per txn. a rmw counts as two opertions
	roTxnP int // percentage of read-only txns
	rwTxnP int // percentage of read-write txns
	cTxnP  int // percentage of customized txns

	keyList    []string // All keys
	z          *Zipf    // Zipfian distribution
	defaultVal string   // default value

	cTxnBuilder CustomizedTxnBuilder
}

func NewYcsb(
	d Driver,
	alpha float64, // negative for uniform distribution
	keyNum, keyLen, valLen int,
	opN, readP, writeP, rmwP int,
	roTxnP, rwTxnP, cTxnP int,
	cTxnType string, // customized txn type
) Workload {
	y := &Ycsb{
		d:       d,
		keyNum:  keyNum,
		keyLen:  keyLen,
		valLen:  valLen,
		opN:     opN,
		readP:   readP,
		writeP:  writeP,
		rmwP:    rmwP,
		roTxnP:  roTxnP,
		rwTxnP:  rwTxnP,
		cTxnP:   cTxnP,
		keyList: make([]string, keyNum, keyNum),
		z:       NewZipf(keyNum, alpha),
	}
	if keyNum < 0 || y.keyLen < 0 || y.valLen < 0 || y.rwTxnP < 0 || y.roTxnP < 0 || y.cTxnP < 0 || y.readP < 0 || y.writeP < 0 || y.rmwP < 0 || y.opN < 0 {
		log.Fatalf("There should be no negative parameters other than zipf alpha")
	}
	// Check percentage
	if y.rwTxnP > 0 {
		y.writeP += y.readP
		y.rmwP += y.writeP // sum <= 100
		if y.rmwP > MAX_PERCENTAGE {
			log.Fatalf("Invalid operation percentage: read %d write %d rmw %d", readP, writeP, rmwP)
		}
	}
	y.rwTxnP += y.roTxnP
	y.cTxnP += y.rwTxnP // sum <= 100
	if y.cTxnP > MAX_PERCENTAGE {
		log.Fatalf("Invalid txn percentage: read-only %d read-write %d customized %d", roTxnP, rwTxnP, cTxnP)
	}

	// Generage keys
	for i := 0; i < keyNum; i++ {
		y.keyList[i] = IntToStrN(i, keyLen)
	}

	y.defaultVal = GenStr(valLen)

	// Customized txn type
	switch cTxnType {
	case YCSB_HALF_RW_TXN:
		y.cTxnBuilder = &HalfRwTxnBuilder{y}
	case YCSB_PARALLEL_RW_TXN:
		y.cTxnBuilder = &ParallelRwTxnBuilder{y.opN, y}
	default:
		log.Fatalf("Invalid ycsb customized txn type %s", cTxnType)
	}

	return y
}

func (y *Ycsb) LoadDB() {
	for _, k := range y.keyList {
		y.loadKV(k, y.defaultVal)
	}
}

func (y *Ycsb) loadKV(k, v string) {
	t := y.d.BeginTxn()
	t.Write(k, v, []uint64{0})
	t.Commit()
}

func (y *Ycsb) GenTxn() TxnInfo {
	r := rand.Intn(MAX_PERCENTAGE)
	if r < y.roTxnP {
		//fmt.Println("generate a read-only txn")
		return y.buildReadOnlyTxn()
	} else if r < y.rwTxnP {
		//fmt.Println("generate a read-write txn")
		return y.buildRWTxn()
	} else if r < y.cTxnP {
		//fmt.Println("generate a customized txn")
		return y.buildCustomizedTxn()
	} else {
		log.Fatalf("No transaction for percentage %d", r)
		return nil
	}
}

func (y *Ycsb) buildReadOnlyTxn() TxnInfo {
	t := NewYcsbTxn(y.d, y.opN)
	for i := 0; i < y.opN; i++ {
		t.addOp(newYcsbOp(YCSB_READ_OP, y.genKey(), ""))
	}
	return t
}

func (y *Ycsb) buildRWTxn() TxnInfo {
	t := NewYcsbTxn(y.d, y.opN)
	for i := 0; i < y.opN; i++ {
		r := rand.Intn(MAX_PERCENTAGE)
		k := y.genKey()
		if r < y.readP {
			t.addOp(newYcsbOp(YCSB_READ_OP, k, ""))
		} else if r < y.writeP {
			v := GenStr(y.valLen)
			t.addOp(newYcsbOp(YCSB_WRITE_OP, k, v))
		} else if r < y.rmwP {
			t.addOp(newYcsbOp(YCSB_READ_OP, k, ""))
			v := GenStr(y.valLen)
			t.addOp(newYcsbOp(YCSB_WRITE_OP, k, v))
			i++ // added two operations. i.e., a rmw counts two operations
			//t.addOp(newYcsbOp(YCSB_RMW_OP, k, v))
		} else {
			log.Fatalf("No operation for percentage %d", r)
		}
	}
	return t
}

func (y *Ycsb) buildCustomizedTxn() TxnInfo {
	return y.cTxnBuilder.BuildTxn()
}

func (y *Ycsb) genKey() string {
	return y.keyList[y.z.Rand()]
}

type CustomizedTxnBuilder interface {
	BuildTxn() TxnInfo
}

// Half reads and half writes
type HalfRwTxnBuilder struct {
	y *Ycsb
}

func (b *HalfRwTxnBuilder) BuildTxn() TxnInfo {
	//fmt.Println("Build a half-rw txn")
	t := NewYcsbTxn(b.y.d, b.y.opN)
	for i := 0; i < b.y.opN; i++ {
		k := b.y.genKey()
		if i%2 == 0 {
			t.addOp(newYcsbOp(YCSB_READ_OP, k, ""))
		} else {
			v := GenStr(b.y.valLen)
			t.addOp(newYcsbOp(YCSB_WRITE_OP, k, v))
		}
	}
	return t
}

type ParallelRwTxnBuilder struct {
	opN int // number of reads followed by the same number of writes on the same set of keys
	y   *Ycsb
}

func (b *ParallelRwTxnBuilder) BuildTxn() TxnInfo {
	v := GenStr(b.y.valLen)
	kList := make([]string, b.opN, b.opN)
	for i := 0; i < b.opN; i++ {
		kList[i] = b.y.genKey()
	}
	t := &ParallelRwTxn{b.y.d, kList, v}
	return t
}

/*
//// TODO Remove deprecated functions

//// DEPRECATED
//// Read-only transactins
// Parameters: number of reads
func (y *Ycsb) ReadOnlyTxn(rN int) {
	t := y.d.BeginTxn()
	for i := 0; i < rN; i++ {
		key := y.genKey()
		t.Read(key)
	}
	t.Commit()
}

//// DEPRECATED
//// Write-only transactions
// Parameters: number of writes
func (y *Ycsb) WriteOnlyTxn(wN int, val string) {
	t := y.d.BeginTxn()
	for i := 0; i < wN; i++ {
		key := y.genKey()
		t.Write(key, val, []uint64{0})
	}
	t.Commit()
}

//// DEPRECATED
//// Read-modify-write transactions
// Parameters: number of read-modify-write operations
func (y *Ycsb) ReadWriteTxn(n int, val string) {
	t := y.d.BeginTxn()
	for i := 0; i < n; i++ {
		key := y.genKey()
		_, commitPoint := t.Read(key)
		t.Write(key, val, []uint64{commitPoint})
	}
	t.Commit()
}

//// DEPRECATED
//// Customized transactions
func (y *Ycsb) Read2Write2Txn(val string) {
	rN, wN := 2, 2 // number of reads and writes
	t := y.d.BeginTxn()
	kList := make([]string, rN, rN)
	resC := make(chan uint64, rN)

	// reads
	for i := 0; i < rN; i++ {
		go func(idx int) {
			key := y.genKey()
			kList[idx] = key
			_, cp := t.Read(key)
			resC <- cp
		}(i)
	}
	cpList := make([]uint64, rN)
	i := 0
	for cp := range resC {
		cpList[i] = cp
		i++
		if i >= rN-1 {
			break
		}
	}

	// writes
	var wg sync.WaitGroup
	// assummes rN == wN
	for i := 0; i < wN; i++ {
		wg.Add(1)
		go func(idx int) {
			t.Write(kList[idx], val, cpList)
			wg.Done()
		}(i)
	}
	wg.Wait() // blocks until all writes return

	t.Commit()
}
*/
