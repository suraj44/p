package workload

const (
	MAX_PERCENTAGE = 100

	YCSB_READ_OP  = 1
	YCSB_WRITE_OP = 2
	//YCSB_RMW_OP   = 3

	YCSB_HALF_RW_TXN     = "halfrw"
	YCSB_PARALLEL_RW_TXN = "parallelrw"
)
