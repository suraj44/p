package workload

import (
	"math/rand"
	"strconv"
)

// Characters
const cSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

//// Helper functions for generating keys and/or values
func SetRandomSeed(s int64) {
	rand.Seed(s)
}

// Generate a set of strings, where each string has the given length
func GenStrList(num, length int) []string {
	sList := make([]string, num, num)
	for i := 0; i < num; i++ {
		sList[i] = GenStr(length)
	}

	return sList
}

func GenStr(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = cSet[rand.Intn(len(cSet))]
	}
	return string(b)
}

func Str(i int) string {
	return strconv.Itoa(i)
}

// Returns a string of the given int in the given length
// If the length is smaller than the given int string, returns the string
func IntToStrN(n, length int) string {
	res := strconv.Itoa(n)
	l := length - len(res)
	for i := 0; i < l; i++ {
		res = "0" + res
	}
	return res
}
