package workload

// TODO Design a general workload interface to cover Retwis and other workloads
type Workload interface {
	LoadDB()         // Load init data to the storage system
	GenTxn() TxnInfo // Generage a transaction instance
}

type TxnInfo interface {
	Exec() TxnRes // Executes a transaction, and returns the execution result
}

type TxnRes interface {
	IsCommitted() bool
	GetVal() string
}

type DefaultTxnRes struct {
	isCommit bool
}

func NewDefaultTxnRes(isCommit bool) TxnRes {
	return &DefaultTxnRes{isCommit}
}

func (t *DefaultTxnRes) IsCommitted() bool {
	return t.isCommit
}

func (t *DefaultTxnRes) GetVal() string {
	return ""
}
