package workload

import (
	"log"
	"sync"
)

//// Read-only and read-write transactions
type YcsbTxn struct {
	d      Driver
	opList []*ycsbOp
}

func NewYcsbTxn(d Driver, n int) *YcsbTxn {
	t := &YcsbTxn{d, make([]*ycsbOp, 0, n)}
	return t
}

func (t *YcsbTxn) addOp(op *ycsbOp) {
	t.opList = append(t.opList, op)
}

func (t *YcsbTxn) Exec() TxnRes {
	txn := t.d.BeginTxn()
	// An operation depends on the commit point of the immediately previous operation
	var lastCp uint64 = 0 // the commit point of the immediately previous operation
	for _, op := range t.opList {
		switch op.t {
		case YCSB_READ_OP:
			var err string
			_, lastCp, err = txn.Read(op.k)
			if err != "" {
				txn.Abort()
				return NewDefaultTxnRes(false)
			}
			//fmt.Println("Read", op.k, lastCp)
		case YCSB_WRITE_OP:
			//fmt.Println("Write", op.k, lastCp)
			lastCp, _ = txn.Write(op.k, op.v, []uint64{lastCp})
			//fmt.Println("Done Write", op.k, lastCp)
		//case YCSB_RMW_OP:
		//	_, lastCp = txn.Read(op.k)
		//	lastCp, _ = txn.Write(op.k, op.v, []uint64{lastCp})
		default:
			log.Fatalf("Invalid ycsb operation type: %d", op.t)
		}
	}
	isCommit := txn.Commit()
	return NewDefaultTxnRes(isCommit)
}

//// Customized transactions
type ParallelRwTxn struct {
	d     Driver
	kList []string // a list of read-write keys
	val   string   // write value
}

func (t *ParallelRwTxn) Exec() TxnRes {
	n := len(t.kList)
	cpList := make([]uint64, n, n) // the commit points of reads
	var wg sync.WaitGroup

	txn := t.d.BeginTxn()

	// // reads
	// for i, k := range t.kList {
	// 	wg.Add(1)
	// 	go func(idx int, key string) {
	// 		_, cp := txn.Read(key)
	// 		cpList[idx] = cp
	// 		wg.Done()
	// 	}(i, k)
	// }
	// wg.Wait() // Waits until all reads complete

	// writes
	for _, k := range t.kList {
		wg.Add(1)
		go func(key string) {
			txn.Write(key, t.val, cpList) // A write depends on all pervious reads
			wg.Done()
		}(k)
	}
	wg.Wait() // blocks until all writes return

	isCommit := txn.Commit()
	return NewDefaultTxnRes(isCommit)
}

//// Helper data structures and functions
type ycsbOp struct {
	t int // type
	k string
	v string
}

func newYcsbOp(t int, k, v string) *ycsbOp {
	return &ycsbOp{t, k, v}
}
