package workload

type Txn interface {
	Read(key string) (string, uint64, string)
	Write(key, val string, dependencies []uint64) (uint64, bool)
	Commit() bool
	Abort()
}
