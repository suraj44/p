package workload

// DB interface
type Driver interface {
	BeginTxn() Txn // Returns a transaction handler
}
