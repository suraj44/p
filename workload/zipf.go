package workload

import (
	"math"
	"math/rand"
)

//// Zipfian distribution
type Zipf struct {
	num   int       // total number of samples in the Zipfian distribution
	alpha float64   // Zipfian distribution constant coefficient
	p     []float64 // the probability of each element, accumulated: p[2]=P(1)+P(2)
}

// If alpha < 0, uses uniform distribution
func NewZipf(num int, alpha float64) *Zipf {
	z := &Zipf{
		num:   num,
		alpha: alpha,
		p:     make([]float64, num),
	}
	// Inits the probability for each element (accumulated)
	var c float64 = 0.0
	for i := 1; i <= z.num; i++ {
		c = c + (1.0 / math.Pow(float64(i), z.alpha))
	}
	c = 1.0 / c
	var sum float64 = 0.0
	for i := 1; i <= z.num; i++ {
		sum += (c / math.Pow(float64(i), z.alpha))
		z.p[i-1] = sum
	}
	return z
}

// Acknowledgement: this function is based on TAPIR's Zipfian implementation.
func (z *Zipf) Rand() int {
	if z.alpha < 0 {
		// Uniform selection of keys.
		return rand.Intn(z.num)
	} else {
		// Zipf-like selection of keys.
		var rndNum float64 = 0.0
		for rndNum == 0.0 {
			rndNum = rand.Float64() //[0.0,1.0)
		}

		// Uses binary search to find the key's index
		l, r, mid := 0, z.num, 0
		for l < r {
			if rndNum > z.p[mid] {
				l = mid + 1
			} else if rndNum < z.p[mid] {
				r = mid - 1
			} else {
				break
			}
			// Updates the mid for the last round
			mid = (l + r) / 2
		}

		if mid >= len(z.p) {
			mid = len(z.p) - 1
		}

		if z.p[mid] < rndNum {
			// Takes the right one
			if mid+1 < len(z.p) {
				return mid + 1
			}
		}
		return mid
	}
}
