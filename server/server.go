package main

import (
	"context"
	"fmt"
	"log"
	"sort"
	"sync/atomic"

	"github.com/suraj44/graph-kv/common"
	"github.com/suraj44/graph-kv/db/db"
	"github.com/suraj44/graph-kv/indexer"
	"github.com/suraj44/graph-kv/shard"
	"github.com/suraj44/graph-kv/store"
	"google.golang.org/grpc"
)

const ( // iota is reset to 0
	COLLECT     = iota // c0 == 0
	DISSEMINATE = iota // c1 == 1
	RECONCILE   = iota // c2 == 2
	FLATTEN     = iota
	WAIT        = iota
)

type Server struct {
	store       *store.Store
	addr        string
	peers       []string
	indexer     shard.Indexer
	epochChan   chan int
	epochSize   int
	currSize    int
	stage       uint32
	buffer      []*store.NodeMetadata
	localBuffer []*store.NodeMetadata
	orphanNodes []*store.NodeMetadata
	recvCount   int32
	ackCount    int32
}

func NewServer(addr string, peers []string, epochSize uint64) *Server {
	s := &Server{
		store:     store.NewStore(epochSize),
		addr:      addr,
		peers:     peers,
		indexer:   indexer.New(),
		epochSize: int(epochSize),
		epochChan: make(chan int),
		stage:     0,
		currSize:  0,
		recvCount: 0,
		ackCount:  0,
	}

	// Register the peers for the consistent hashing logic
	s.indexer.SetBuckets(s.peers)
	// go s.BackgroundRoutine()

	return s
}

func (s *Server) StateMachine() bool {
	// tryLock()

	if !atomic.CompareAndSwapUint32(&s.stage, COLLECT, DISSEMINATE) {
		fmt.Printf("%s: Expected stage 0 got stage %d. Could not start dissemination.\n", s.addr, s.stage)
		return false
	}
	log.Println(s.addr, "Started state machine")
	s.Broadcast()
	// log.Println(s.addr, "Done broadcasting")
	// wait to receive metadata from all peers
	peerCount := len(s.peers) - 1
	for {
		if int(s.recvCount) == len(s.peers)-1 {
			break
		}
	}
	// log.Println(s.addr, "Received all metadata")
	// Move to flattening stage
	s.recvCount = 0
	for {
		if atomic.CompareAndSwapUint32(&s.stage, DISSEMINATE, FLATTEN) {
			break
		}
	}
	// insert buffered data into our instance of the storage system and empty the buffer in the process
	// log.Printf("%s: saving nodes %v\n", s.addr, s.buffer)
	s.SavePeerNodes(s.buffer)
	s.buffer = append(s.buffer, s.localBuffer...)

	s.AddOldOrphans()
	s.RemoveOrphans()

	s.buffer = s.GetTopologicalOrder(s.buffer)
	// flatten
	// Flatten(s.buffer)
	// log.Printf("%s: Buffer: %v\n", s.addr, s.buffer)
	// log.Println(s.addr, "Flattening...")
	s.store.ConstructTree(s.buffer)
	//fmt.Println("flatten return:", ret)
	s.buffer = []*store.NodeMetadata{}
	s.recvCount = 0
	// move to stage 3
	for {
		if atomic.CompareAndSwapUint32(&s.stage, FLATTEN, WAIT) {
			break
		}
	}
	// log.Println(s.addr, "Notifying..")
	s.notify()
	// wait for n-1 ACKs
	for {
		if int(s.ackCount) == peerCount {
			break
		}
	}
	log.Println(s.addr, "Received all ACKs. State machine done!")
	// move back to stage 0
	s.recvCount = 0
	s.ackCount = 0
	for {
		if atomic.CompareAndSwapUint32(&s.stage, WAIT, COLLECT) {
			break
		}
	}

	return true
}

func (s *Server) RemoveOrphans() {
	i := 0 // output index
	for _, node := range s.buffer {
		orphan := false
		for _, parent := range node.Dependencies {
			p := s.store.GetNode(parent)
			if p == nil {
				orphan = true
				s.orphanNodes = append(s.orphanNodes, node)
				break
			}
		}
		if !orphan {
			s.buffer[i] = node
			i++
		}
	}
	println("Size of orphans is", len(s.orphanNodes))

	// prevent memory leak
	for j := i; j < len(s.buffer); j++ {
		s.buffer[j] = nil
	}
	s.buffer = s.buffer[:i]
}

func (s *Server) AddOldOrphans() {
	s.buffer = append(s.buffer, s.orphanNodes...)
	for i := range s.orphanNodes {
		s.orphanNodes[i] = nil
	}
	s.orphanNodes = s.orphanNodes[:0]
}

func (s *Server) GetTopologicalOrder(order []*store.NodeMetadata) []*store.NodeMetadata {
	deps := make(map[uint64]struct{})
	var queue []*store.NodeMetadata
	commitPointerMap := make(map[uint64]*store.NodeMetadata)
	// create a set of all the dependencies
	for _, node := range order {
		for _, parent := range node.Dependencies {
			if _, ok := deps[parent]; !ok {
				deps[parent] = struct{}{}
			}
			commitPointerMap[node.Commit] = node
		}
	}

	// record all nodes that do not feature in the set.
	// they are leaves
	var final []*store.NodeMetadata
	visited := make(map[uint64]struct{})
	for _, node := range order {
		if node == nil {
			panic("NIL NODE NIL NODE")
		}
		if _, ok := deps[node.Commit]; !ok {
			queue = append(queue, node)
			visited[node.Commit] = struct{}{}
		}
	}

	sort.Slice(queue, func(i, j int) bool {
		return queue[i].Commit < queue[j].Commit // this is to make sure all shards process the nodes in the same order so that they end up w same trees
	})

	final = append(final, queue...)

	for {
		if len(queue) == 0 {
			break
		}

		currNode := queue[0]
		queue = queue[1:]

		// fmt.Println(currNode)
		// fmt.Println(currNode.Dependencies)

		for _, parent := range currNode.Dependencies {
			if _, ok := visited[parent]; !ok {
				parentNode, ok := commitPointerMap[parent]
				if ok { // if not ok, then those nodes aren't part of the epoch and won't be part of the map - wrong assumption. They could also not even be present on the system! need to verify this as well...
					queue = append(queue, parentNode)
					visited[parent] = struct{}{}
					final = append(final, parentNode)
				}

			}
		}
	}

	// reverse to get topological order
	for i, j := 0, len(final)-1; i < j; i, j = i+1, j-1 {
		final[i], final[j] = final[j], final[i]
	}

	return final
}

func (s *Server) BackgroundRoutine() {
	for { // n := range s.epochChan {
		select {
		case n := <-s.epochChan: // if the if-statement if true, this buffer would become huge. Does this need to be addressed?
			if n == 0 {
				s.currSize++
				if s.currSize >= int(s.epochSize) {
					if s.stage == 0 && s.StateMachine() {
						// s.currSize -= s.epochSize
						if s.currSize < 0 {
							s.currSize = 0
						}
					}
					// Call Disseminate()
				}
				// }
				// } else if n == 1 {
				// 	fmt.Println("Consolidating...")
				// 	s.ConstructTree(true)
				// 	s.routineFinish = true
				// 	break
			} else if n == 1 {
				s.StateMachine()
			}

		}
	}
}

func (s *Server) notify() {
	for _, server := range s.peers {
		if server != s.addr {
			// RPC to send data to peer
			ctx := context.Background()
			conn, err := grpc.Dial(server, grpc.WithInsecure())
			if err != nil {
				panic(err)
			}
			client := db.NewDbServiceClient(conn)
			// TODO: add error check
			client.Notify(ctx, &db.ACK{
				Addr: s.addr,
			})
			conn.Close()
		}
	}

}

func (s *Server) Flatten() {

}

func (s *Server) SavePeerNodes(list []*store.NodeMetadata) {
	// fmt.Println("started save")
	for _, node := range list {
		err := s.store.Save(node.Commit, node.Key, node.Tid, node.Dependencies)
		if err != nil {
			panic(err)
		}
		// s.epochChan <- 0
	}
	// fmt.Println("finished save")
}

func (s *Server) Broadcast() bool {

	// iterate thru queue from storage system and append data to local snapshot
	// fmt.Printf("epochSize is %d\n", s.epochSize)
	localSnapshot := s.store.ReleaseBuffer(s.epochSize)
	s.currSize -= len(localSnapshot)
	// fmt.Printf("%s: size of local snapshot is %d\n", s.addr, len(localSnapshot))
	s.localBuffer = localSnapshot
	metadata := make([]*db.DisseminateRequest, len(localSnapshot))

	// would using an approach of marshaling and unmarshaling to bytes[] be better?
	// segfaulting here. why???
	for i := range localSnapshot {
		m := &db.DisseminateRequest{
			Commit:       localSnapshot[i].Commit,
			Dependencies: localSnapshot[i].Dependencies,
			Key:          localSnapshot[i].Key,
			Tid:          localSnapshot[i].Tid,
		}
		metadata[i] = m
		// metadata[i].Dependencies = localSnapshot[i].Dependencies
		// metadata[i].Key = localSnapshot[i].Key
		// metadata[i].Tid = localSnapshot[i].Tid

	}

	//s.buffer = append(s.buffer, localSnapshot) // not needed since these nodes are already present on our instance of the system!

	// broadcast local snapshot to other servers
	for _, server := range s.peers {
		if server != s.addr {
			// fmt.Printf("%s: sending data to %s\n", s.addr, server)
			// RPC to send data to peer
			ctx := context.Background()
			conn, err := grpc.Dial(server, grpc.WithInsecure())
			if err != nil {
				panic(err)
				return false
			}
			client := db.NewDbServiceClient(conn)
			// TODO: add error check
			client.Disseminate(ctx, &db.DisseminateRequestList{
				Metadata: metadata,
			})
			conn.Close()
		}
	}

	// wait and process received data

	// call tree flattening

	return true
}

func (s *Server) BroadcastParents(missingParents []*store.NodeMetadata) bool {
	metadata := make([]*db.DisseminateRequest, len(missingParents))

	// would using an approach of marshaling and unmarshaling to bytes[] be better?
	// segfaulting here. why???
	for i := range missingParents {
		m := &db.DisseminateRequest{
			Commit:       missingParents[i].Commit,
			Dependencies: missingParents[i].Dependencies,
			Key:          missingParents[i].Key,
			Tid:          missingParents[i].Tid,
		}
		metadata[i] = m
	}

	// broadcast missing parents to other servers
	for _, server := range s.peers {
		if server != s.addr {
			ctx := context.Background()
			conn, err := grpc.Dial(server, grpc.WithInsecure())
			if err != nil {
				panic(err)
			}
			client := db.NewDbServiceClient(conn)
			// TODO: add error check
			client.MissingParents(ctx, &db.DisseminateRequestList{
				Metadata: metadata,
			})
			conn.Close()
		}
	}

	return true
}

func (s *Server) Set(ctx context.Context, req *db.SetRequest) (result *db.SetResponse, err error) {
	// log.Printf("%s: Set request: key: %s value: %v dependencies: %v\n", s.addr, req.Key, req.Value, req.Dependencies)

	commit, _ := s.store.Set(req.Key, req.Value, req.TId, req.Dependencies)

	// TODO: figure out why using err from kv.Set() results in SIGSEGV
	result = &db.SetResponse{
		Commit: commit,
		Error:  "",
	}
	// fmt.Println("stuck??")
	// s.epochChan <- 0
	go func() {
		s.epochChan <- 0
	}()
	err = nil

	// TODO: figure out why using err from kv.Set() results in SIGSEGV
	return result, err
}

func (s *Server) GetbyKey(ctx context.Context, req *db.GetRequest) (result *db.GetResponse, err error) {
	// log.Printf("%s: Get request: key: %s\n", s.addr, req.Key)

	value, commit, _ := s.store.GetbyKey(req.Key, 0)

	// TODO: figure out why using err from kv.Set() results in SIGSEGV
	result = &db.GetResponse{
		Value:  value,
		Commit: commit,
		Error:  "",
	}
	// go func() {
	// 	s.epochChan <- 0
	// }()
	err = nil

	return result, err
}

func (s *Server) Print(ctx context.Context, req *db.Empty) (result *db.Empty, err error) {
	log.Printf("%s: Received print request\n", s.addr)
	s.store.Print()
	return &db.Empty{}, nil
}

func (s *Server) Disseminate(ctx context.Context, req *db.DisseminateRequestList) (*db.Empty, error) {
	// fmt.Printf("%s: Disseminate received\n", s.addr)
	if s.stage != FLATTEN {
		var respList []*store.NodeMetadata
		// respList := make([]*store.NodeMetadata, len(req.Metadata))
		for i := range req.Metadata {
			element := &store.NodeMetadata{
				Commit:       req.Metadata[i].Commit,
				Dependencies: req.Metadata[i].Dependencies,
				Key:          req.Metadata[i].Key,
				Tid:          req.Metadata[i].Tid,
			}

			s.buffer = append(s.buffer, element)
		}
		// TODO: Add a lock here cause there can be concurrent inserts.
		s.buffer = append(s.buffer, respList...)
		atomic.AddInt32(&s.recvCount, 1)
		if s.stage == COLLECT {
			go s.StateMachine()
		} else if s.stage == WAIT {
			s.epochChan <- 1
		}
	}

	return &db.Empty{}, nil
}

func (s *Server) MissingParents(ctx context.Context, req *db.DisseminateRequestList) (*db.Empty, error) {
	// fmt.Printf("%s: Disseminate received\n", s.addr)
	if s.stage == RECONCILE {
		var respList []*store.NodeMetadata
		// respList := make([]*store.NodeMetadata, len(req.Metadata))
		for i := range req.Metadata {
			element := &store.NodeMetadata{
				Commit:       req.Metadata[i].Commit,
				Dependencies: req.Metadata[i].Dependencies,
				Key:          req.Metadata[i].Key,
				Tid:          req.Metadata[i].Tid,
			}

			s.buffer = append(s.buffer, element)
		}
		// TODO: Add a lock here cause there can be concurrent inserts.
		s.SavePeerNodes(respList)
		s.buffer = append(s.buffer, respList...)
		atomic.AddInt32(&s.recvCount, 1)
	}

	return &db.Empty{}, nil
}

func (s *Server) Notify(ctx context.Context, req *db.ACK) (*db.Empty, error) {
	if int(s.ackCount) == len(s.peers)-1 {
		return &db.Empty{}, common.ErrExcessAck
	}

	atomic.AddInt32(&s.ackCount, 1)

	return &db.Empty{}, nil

}
