package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"strings"

	_ "net/http/pprof"

	"github.com/suraj44/graph-kv/db/db"
	"google.golang.org/grpc"
)

var epochSize = flag.Uint64("epochSize", 10000, "size of an epoch for tree flattening")
var serverAddress = flag.String("address", "10.0.3.5:5000", "address of the server")
var peers = flag.String("peers", "", "comma separated list of peer nodes")

func main() {
	flag.Parse()
	// runtime.SetBlockProfileRate(1)

	// if strings.Contains(*serverAddress, "10.0.1.5") {
	// 	go func() {
	// 		http.ListenAndServe("10.0.1.5:3000", nil)
	// 	}()
	// }

	/*
	 * Read IP addresses of peers from user input.
	 * This information would be used when creating
	 * a global snapshot of the system
	 */
	clusterAddrMap := make(map[string]struct{})
	clusterAddrMap[*serverAddress] = struct{}{}

	peerSlice := strings.Split(*peers, ",")
	for _, peer := range peerSlice {
		clusterAddrMap[peer] = struct{}{}
	}

	fmt.Println(clusterAddrMap)

	server := NewServer(*serverAddress, peerSlice, *epochSize)
	// lis, err := server.Init(*serverAddress, clusterAddrMap, *shardCount, 1)
	// if err != nil {
	// 	log.Fatalf("failed to listen: %v", err)
	// }

	lis, err := net.Listen("tcp", server.addr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
		return
	}
	log.Println("Server listen at", server.addr)

	grpcServer := grpc.NewServer()
	db.RegisterDbServiceServer(grpcServer, server)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
		return
	}

}
